<?php 
/**
 * Template Name: Page Rendez-vous
 */
 ?>

<?php get_header(); ?>
        <main>
            <section class="blcRdv blc_page">
                <div class="container">
                    <div class="introText">
                        <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms">
                            <!-- VOTRE RENDEZ-VOUS EN LIGNE -->
                            <?php the_field('titre') ?>
                            <!--<span>Lorem ipsum dolor sit amet, consectetur adipisicing elit.</span>-->
                            <span><?php the_field('sous-titre') ?></span>
                        </h2>
                        <div class="wow fadeInUp" data-wow-delay="800ms">
                            <!--
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>-->
                            <?php the_field('description') ?>
                        </div>
                    </div>
                </div>         
            </section>

            <section class="etapeRDV">
                <div class="container">
                    <form id="form_devis" enctype="multipart/form-data">
                        <div class="lst-etape">
                            <div class="etape etape1" >
                                <div class="titre">
                                    <h3><span>étape 01/07</span></h3>
                                    <h2>Vous êtes ?</h2>
                                </div>
                                <div class="liste-option immeuble status">
                                    <ul>
                                        <li>
                                            <input id="locataire" name="status" value="Locataire"  type="radio">
                                            <label for="locataire" class="locataire next">Locataire</label>
                                            <div class="check"></div>
                                        </li>
                                        <li>
                                            <input id="bailleur" name="status" value="Bailleur" type="radio">
                                            <label for="bailleur" class="bailleur next">Bailleur</label>
                                            <div class="check"></div>
                                        </li>
                                        <li >
                                            <input id="professionnel" name="status" value="Professionnel" type="radio">
                                            <label for="professionnel" class="professionnel next">Professionnel</label>
                                            <div class="check"></div>
                                        </li>
                                        <li >
                                            <input id="gestionnaire" name="status" value="Gestionnaire" type="radio">
                                            <label for="gestionnaire" class="gestionnaire next ">Gestionnaire</label>
                                            <div class="check"></div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="etape etape2" id="etape2" >
                                <div class="titre">
                                    <h3><span>étape 02/07</span></h3>
                                    <h2>Quelle type de mission ?</h2>
                                </div>
                                <div class="liste-option mission">
                                    <ul>
                                        <?php
                                            $missions = get_custom_posts('mission');
                                            $i = 0;
                                            if ( $missions->have_posts() ) : while ( $missions->have_posts() ) : $missions->the_post();
                                                $mission_ID   = get_the_ID();
                                                $idf_mission  = 'mission'.$i;
                                                $titre_front  = get_field('titre_front');
                                                $icone        = get_field('icone');
                                                $icone_hover  = get_field('icone_hover');
                                                $avec_options = get_field('avec_options');
                                                if($avec_options == 'non'){ $class_option = 'without_option'; }else { $class_option=''; }
                                        ?>
                                        <style>
                                            .liste-option ul li label.<?= $idf_mission; ?>:after { background-image: url(<?= $icone; ?>); }
                                            .liste-option input[type=radio]:checked ~ label.<?= $idf_mission; ?>:after,
                                            .liste-option li:hover label.<?= $idf_mission; ?>:after { background-image: url(<?= $icone_hover; ?>); }
                                        </style>
                                        <li>
                                            <input id="<?= $idf_mission; ?>" class="<?= $class_option; ?>" name="type_mission" type="radio" value="<?= $mission_ID; ?>">
                                            <label for="<?= $idf_mission; ?>" class="<?= $idf_mission; ?>"><?php the_title(); ?></label>
                                            <div class="check"></div>
                                        </li>
                                        <?php $i++; endwhile; wp_reset_query(); endif; ?> 

                                    </ul>
                                </div>
                            </div>
                            <div class="etape etape3" id="etape3" >
                                <div class="titre">
                                    <h3><span>étape 03/07</span></h3>
                                    <h2>Quel type d’immeuble</h2>
                                </div>
                                <div class="liste-option">
                                    <ul>
                                        <?php 
                                            $immeubles = get_custom_posts('immeuble');
                                            $i = 0;
                                            if ( $immeubles->have_posts() ) : while ( $immeubles->have_posts() ) : $immeubles->the_post();
                                                $immeuble_ID   = get_the_ID();
                                                $idf_immeuble  = 'immeuble'.$i;
                                                $titre_front  = get_field('titre_front');
                                                $icone        = get_field('icone');
                                                $icone_hover  = get_field('icone_hover');
                                                $popup_contact  = get_field('popup_contact');
                                                $css_id = $post->post_name;
                                        ?>

                                            <?php if($popup_contact == 'oui') : ?>
                                                <style>
                                                    .liste-option ul li .<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone; ?>); }
                                                    .liste-option input[type=radio]:checked ~ .<?= $idf_immeuble; ?>:after,
                                                    .liste-option li:hover .<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone_hover; ?>); }
                                                </style>
                                                <li>
                                                    <a href="#popup_contact" class="open-popup-link <?= $idf_immeuble; ?>"><?= $titre_front; ?></a>
                                                    <div class="check"></div>                                  
                                                </li> 
                                            <?php else : ?>
                                                <style>
                                                    .liste-option ul li label.<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone; ?>); }
                                                    .liste-option input[type=radio]:checked ~ label.<?= $idf_immeuble; ?>:after,
                                                    .liste-option li:hover label.<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone_hover; ?>); }
                                                </style>
                                                <li id="<?= $css_id ?>">
                                                    <input id="<?= $idf_immeuble; ?>" name="type_immeuble" type="radio" value="<?= $immeuble_ID; ?>" >
                                                    <label for="<?= $idf_immeuble; ?>" class="<?= $idf_immeuble; ?>"><?= $titre_front; ?></label>
                                                    <div class="check"></div>
                                                </li>
                                            <?php endif; ?>

                                        <?php $i++; endwhile; wp_reset_query(); endif; ?>                                    
                                        
                                    </ul>
                                </div>

                                <div class="autre-option" style="display: none;">
                                    <h2>Autres options</h2>
                                    <div class="lst-Autre-option clr">
                                        <div class="col col-33">
                                            <div class="blc-chp clr" id="o_nbr_chambre">
                                                <label>Nombre de chambres</label>
                                                <div class="numbers-row">
                                                    <button class="dec button" id="boutton1" type="submit">-</button>
                                                    <input type="text" name="nb_chambre" id="qtt_chambre" value="0" class="qtt">
                                                    <button class="inc button"  id="boutton2">+</button>
                                                    <div class="tooltip">
                                                        <span> Un bureau est considéré comme un chambre</span>
                                                    </div> 
                                                   
                                                </div>
                                            </div>
                                            <div class="blc-chp clr" id="o_nbr_sdb">
                                                <label>Nombre de salles de bains</label>
                                                <div class="numbers-row">
                                                    <button class="dec button"  id="boutton3">-</button>
                                                    <input type="text" name="nb_salle_bain" id="qtt_sallebain" value="0" class="qtt">
                                                    <button class="inc button"  id="boutton4" type="submit">+</button>
                                                </div>
                                            </div>
                                            <div class="blc-chp clr" id="o_surface">
                                                <label>Surface approximative (en m²) *</label>
                                                <div class="chp">
                                                    <input type="" name="surface" class="chp" id="surface">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-66 commentaire">
                                            <div class="blc-check">
                                                <ul>
                                                    <?php 
                                                        $autre_options = get_custom_posts('autre_option');
                                                        $i = 0;
                                                        if ( $autre_options->have_posts() ) : while ( $autre_options->have_posts() ) : $autre_options->the_post();
                                                            $autre_option_ID   = get_the_ID();
                                                            $idf_autre_option  = 'autre_option'.$i;
                                                            $texte_indicatif = get_field('texte_indicatif');
                                                    ?>
                                                    <li id="o_<?= $i ?>">
                                                        <input id="<?= $idf_autre_option; ?>" name="options[]" type="checkbox" value="<?= $autre_option_ID; ?>">
                                                        <label for="<?= $idf_autre_option; ?>"><?php the_title(); ?></label>
                                                        <div class="check"></div>
                                                        <?php if($texte_indicatif): ?>
                                                            <div class="tooltip">
                                                                <span><?= $texte_indicatif; ?></span>
                                                            </div>
                                                        <?php endif; ?>
                                                    </li>
                                                    <?php $i++; endwhile; wp_reset_query(); endif; ?>
                                                </ul>
                                            </div>
                                        </div>

                                        <div class="col col-66" id="com_wrap">
                                            <div class="blc-chp">
                                                <div class="chp textarea" id="commentaire">
                                                    <textarea placeholder="Commentaires*" id="comment" name="comment"></textarea>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="clr"></div>
                                        <div class="info" id="o_info"><p>*  Les m² sont obligatoires pour Bureaux , Commerces,<br> Industriel, Bâtiments Publics</p></div>
                                    </div>
                                </div>
                                <div class="blc-btn">
                                    <a href="etape4" class="btn hvr-btn next3 scroll1">étape suivante</a>
                                </div>
                            </div>
                            <div class="etape etape4 " id="etape4" >
                                <div class="titre">
                                    <h3><span>étape 04/07</span></h3>
                                    <h2>Adresse du bien</h2>
                                </div>
                                <div class="searchMap clr">
                                    <div class="col">
                                        <div class="chp search">
                                            <input type="text" name="adresse" placeholder="Indiquez l’adresse de votre bien *" class="txt" id="adressedubien">
                                            <div class="btn-search"></div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <div class="chp search">
                                            <input type="text" name="numeroAppart" placeholder="Numéro d'appartement *" class="txt" id="numeroAppart"> 
                                        </div>
                                    </div>
                                    <!-- <div class="col">
                                        <b>REMISE IMMÉDIATE DE 10€*</b>
                                        <span>* En prenant rendez-vous en ligne</span>
                                    </div> -->
                                </div>
                                <div class="map">
                                    <div id="map"></div>
                                </div>
                                <div class="blc-btn">
                                    <a href="etape5" class="btn hvr-btn next4 scroll1">étape suivante</a>
                                </div>
                            </div>
                            <div class="etape etape5" id="etape5" >
                                <div class="titre">
                                    <h3><span>étape 05/07</span></h3>
                                    <h2>Ajouter des fichiers</h2>
                                </div>
                                <div class="blcFormulaire fichier">
                                    <div class="info">
                                        <p>* Ajouter votre bail, photos du bien, plans,…</p>
                                    </div>
                                    <div class="chp">
                                        <div class="cont-file">
                                        	<span>Aucun fichier sélectionné</span>
                                            <input type="file" name="file[]" class="input-file">
                                            <i> Parcourir</i>
                                            <i class="reset" style="display: none">Supprimer</i>
                                        </div>
                                        <span id="spn_inputs"></span>
                                    </div>
                                    <div class="info">
                                        <p><a href="javascript:void(0);" id="more_files">[Ajouter un autre fichier]</p>
                                    </div>
                                    <div class="blc-btn">
                                        <a href="etape6" class="btn hvr-btn next5 scroll1">étape suivante</a>
                                    </div>
                                </div>
                            </div>
                            <div class="etape etape6" id="etape6" >
                                <div class="titre">
                                    <h3><span>étape 06/07</span></h3>
                                    <h2>Date et heure souhaitées</h2>
                                </div>
                                <div class="blcFormulaire">
                                    <div class="chp date">
                                        <input type="text" name="date_rdv" id="date" placeholder="Date et heure * " class="datetimepicker">
                                        <input type="text" name="heure_rdv" id="time" placeholder=" " class="datetimepicker">
                                    </div>
                                    <div class="fichier">
                                       <div class="info info-date">
                                        <p> *La date et l’heure renseignées correspondent à la date et heure la plus proche souhaitée par le client, sous réserve de disponibilité d’Axis Experts Srl</p>
                                    </div> 
                                    </div>
                                     
                                    <div class="blc-btn">
                                        <a href="etape7" class="btn hvr-btn next6 scroll1">étape suivante</a>
                                    </div>
                                </div>
                            </div>
                            <div class="etape etape7 " id="etape7">
                                <div class="titre">
                                    <h3><span>étape 07/07</span></h3>
                                    <h2>Vos coordonnées</h2>
                                </div>
                                <div class="blcFormulaire ">
                                    <div class="clr step6">
                                        <div class="col">
                                            <div class="chp">
                                                <input type="text" class="form-control" name="name" id="name" required="required" placeholder="Votre Nom *">
                                            </div>
                                            <!--
                                            <div class="chp">
                                                <input type="text" class="form-control" name="prenom" id="firstName" required="required" placeholder="Votre prénom *">
                                            </div>-->
                                        </div>
                                        <div class="col">
                                            <div class="chp">
                                                <input type="mail" class="form-control" name="email" id="mail" required="required" placeholder="Votre adresse e-mail *">
                                            </div>
                                            <!--
                                            <div class="chp">
                                                <input type="text" class="form-control" name="telephone" id="tel" required="required" placeholder="Votre numéro de téléphone *">
                                            </div>-->
                                        </div>
                                        <div class="col">
                                            <div class="chp">
                                                <input type="tel" class="form-control" name="Numéro" id="numero" required="required" placeholder="Votre téléphone *">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blc-btn">
                                        <input type="hidden" name="form_type" value="appointment">
                                        <button class="btn hvr-btn" id="confirm_rdv">Prendre rendez vous</button>
                                    </div>
                                </div>
                                
                                
                                <a href="#pp-confirmation" id="topopup" class="fancybox" data-fancybox="" style="display: none;"></a>
                                <div class="pp-confirmation" id="pp-confirmation" style="display: none;">
                                    <div class="content">
                                        <h2>Confirmation</h2>
                                        <p>Votre demande de rendez-vous a été prise en compte </p>
                                    </div>
                                </div>
                            </div>

                            <!--
                            <div class="etape etape1" >
                                <div class="titre">
                                    <h3><span>étape 01/05</span></h3>
                                    <h2>Quelle type de mission ?</h2>
                                </div>
                                <div class="liste-option mission">
                                    <ul> 
                                        <?php 
                                            $missions = get_custom_posts('mission');
                                            $i = 0;
                                            if ( $missions->have_posts() ) : while ( $missions->have_posts() ) : $missions->the_post();
                                                $mission_ID   = get_the_ID();
                                                $idf_mission  = 'mission'.$i;
                                                $titre_front  = get_field('titre_front');
                                                $icone        = get_field('icone');
                                                $icone_hover  = get_field('icone_hover');
                                                $avec_options = get_field('avec_options');
                                                if($avec_options == 'non'){ $class_option = 'without_option'; }else { $class_option=''; }
                                        ?>
                                        <style>
                                            .liste-option ul li label.<?= $idf_mission; ?>:after { background-image: url(<?= $icone; ?>); }
                                            .liste-option input[type=radio]:checked ~ label.<?= $idf_mission; ?>:after,
                                            .liste-option li:hover label.<?= $idf_mission; ?>:after { background-image: url(<?= $icone_hover; ?>); }
                                        </style>
                                        <li>
                                            <input id="<?= $idf_mission; ?>" class="<?= $class_option; ?>" name="type_mission" type="radio" value="<?= $mission_ID; ?>">
                                            <label for="<?= $idf_mission; ?>" class="<?= $idf_mission; ?>"><?php the_title(); ?></label>
                                            <div class="check"></div>
                                        </li>
                                        <?php $i++; endwhile; wp_reset_query(); endif; ?> 

                                    </ul>
                                </div>
                            </div>
                            <div class="etape etape2" id="etape2" >
                                <div class="titre">
                                    <h3><span>étape 02/05</span></h3>
                                    <h2>Quel type d’immeuble</h2>
                                </div>
                                <div class="liste-option">
                                    <ul>
                                        <?php 
                                            $immeubles = get_custom_posts('immeuble');
                                            $i = 0;
                                            if ( $immeubles->have_posts() ) : while ( $immeubles->have_posts() ) : $immeubles->the_post();
                                                $immeuble_ID   = get_the_ID();
                                                $idf_immeuble  = 'immeuble'.$i;
                                                $titre_front  = get_field('titre_front');
                                                $icone        = get_field('icone');
                                                $icone_hover  = get_field('icone_hover');
                                                $popup_contact  = get_field('popup_contact');
                                        ?>

                                            <?php if($popup_contact == 'oui') : ?>
                                                <style>
                                                    .liste-option ul li .<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone; ?>); }
                                                    .liste-option input[type=radio]:checked ~ .<?= $idf_immeuble; ?>:after,
                                                    .liste-option li:hover .<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone_hover; ?>); }
                                                </style>
                                                <li>
                                                    <a href="#popup_contact" class="open-popup-link <?= $idf_immeuble; ?>"><?= $titre_front; ?></a>
                                                    <div class="check"></div>                                  
                                                </li> 
                                            <?php else : ?>
                                                <style>
                                                    .liste-option ul li label.<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone; ?>); }
                                                    .liste-option input[type=radio]:checked ~ label.<?= $idf_immeuble; ?>:after,
                                                    .liste-option li:hover label.<?= $idf_immeuble; ?>:after { background-image: url(<?= $icone_hover; ?>); }
                                                </style>
                                                <li>
                                                    <input id="<?= $idf_immeuble; ?>" name="type_immeuble" type="radio" value="<?= $immeuble_ID; ?>" >
                                                    <label for="<?= $idf_immeuble; ?>" class="<?= $idf_immeuble; ?>"><?= $titre_front; ?></label>
                                                    <div class="check"></div>
                                                </li>
                                            <?php endif; ?>

                                        <?php $i++; endwhile; wp_reset_query(); endif; ?>                                    
                                        
                                    </ul>
                                </div>

                                <div class="autre-option" style="display: none;">
                                    <h2>Autres options</h2>
                                    <div class="lst-Autre-option clr">
                                        <div class="col col-33">
                                            <div class="blc-chp clr">
                                                <label>Nombre de chambres</label>
                                                <div class="numbers-row">
                                                    <button class="dec button" id="boutton1" type="submit">-</button>
                                                    <input type="text" name="nb_chambre" id="qtt_chambre" value="0" class="qtt">
                                                    <button class="inc button"  id="boutton2">+</button>
                                                    <div class="tooltip">
                                                        <span> Un bureau est considéré comme un chambre</span>
                                                    </div> 
                                                   
                                                </div>
                                            </div>
                                            <div class="blc-chp clr">
                                                <label>Nombre de salles de bains</label>
                                                <div class="numbers-row">
                                                    <button class="dec button"  id="boutton3">-</button>
                                                    <input type="text" name="nb_salle_bain" id="qtt_sallebain" value="0" class="qtt">
                                                    <button class="inc button"  id="boutton4" type="submit">+</button>
                                                </div>
                                            </div>
                                            <div class="blc-chp clr">
                                                <label>Surface approximative (en m²) *</label>
                                                <div class="chp">
                                                    <input type="" name="surface" class="chp" id="surface">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col col-66">
                                            <div class="blc-check">
                                                <ul>
                                                    <?php 
                                                        $autre_options = get_custom_posts('autre_option');
                                                        $i = 0;
                                                        if ( $autre_options->have_posts() ) : while ( $autre_options->have_posts() ) : $autre_options->the_post();
                                                            $autre_option_ID   = get_the_ID();
                                                            $idf_autre_option  = 'autre_option'.$i;
                                                            $texte_indicatif = get_field('texte_indicatif');
                                                    ?>
                                                    <li>
                                                        <input id="<?= $idf_autre_option; ?>" name="options[]" type="checkbox" value="<?= $autre_option_ID; ?>">
                                                        <label for="<?= $idf_autre_option; ?>"><?php the_title(); ?></label>
                                                        <div class="check"></div>
                                                        <?php if($texte_indicatif): ?>
                                                            <div class="tooltip">
                                                                <span><?= $texte_indicatif; ?></span>
                                                            </div>
                                                        <?php endif; ?>
                                                    </li>
                                                    <?php $i++; endwhile; wp_reset_query(); endif; ?> 
                                                    
                                                </ul>
                                            </div>
                                            <div class="blc-chp">
                                                <div class="chp textarea" id="commentaire">
                                                    <textarea placeholder="Commentaires*" id="comment" name="comment"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clr"></div>
                                        <div class="info"><p>*  Les m² sont obligatoires pour Bureaux , Commerces,<br> Industriel, Bâtiments Publics</p></div>

                                    </div>
                                    
                                </div>
                                <div class="blc-btn">
                                    <a href="etape4" class="btn hvr-btn next3 scroll1">étape suivante</a>
                                </div>
                            </div>
                            <div class="etape etape3 " id="etape3" >
                                <div class="titre">
                                    <h3><span>étape 03/05</span></h3>
                                    <h2>Adresse du bien</h2>
                                </div>
                                <div class="searchMap clr">
                                    <div class="col">
                                        <div class="chp search">
                                            <input type="text" name="adresse" placeholder="Indiquez l’adresse de votre bien *" class="txt" id="adressedubien">
                                            
                                            <div class="btn-search"></div>
                                        </div>
                                    </div>
                                    <div class="col">
                                        <b>Tarif spécial WEB.</b>
                                        <span>* Ajoutez l’adresse de votre bien et profitez d’une remise directe de 15%</span>
                                    </div>
                                </div>
                                <div class="map">
                                    <div id="map"></div>
                                </div>
                                <div class="blc-btn">
                                    <a href="etape5" class="btn hvr-btn next4 scroll1">étape suivante</a>
                                </div>
                            </div>
                            <div class="etape etape4" id="etape4" >
                                <div class="titre">
                                    <h3><span>étape 04/05</span></h3>
                                    <h2>Date et heure souhaitées</h2>
                                </div>
                                <div class="blcFormulaire ">
                                    <div class="chp date">
                                        <input type="text" name="date_heure" id="datetimepicker" placeholder="Date et heure * " class="datetimepicker">
                                    </div>
                                    <div class="blc-btn">
                                        <a href="etape6" class="btn hvr-btn next5 scroll1">étape suivante</a>
                                    </div>
                                </div>
                            </div>
                            <div class="etape etape5 " id="etape5">
                                <div class="titre">
                                    <h3><span>étape 05/05</span></h3>
                                    <h2>Vos coordonnées</h2>
                                </div>
                                <div class="blcFormulaire ">
                                    <div class="clr step6">
                                        <div class="col">
                                            <div class="chp">
                                                <input type="text" class="form-control" name="nom" id="name" required="required" placeholder="Votre Nom *">
                                            </div>
                                            <div class="chp">
                                                <input type="text" class="form-control" name="prenom" id="firstName" required="required" placeholder="Votre prénom *">
                                            </div>
                                        </div>
                                        <div class="col">
                                            <div class="chp">
                                                <input type="mail" class="form-control" name="email" id="mail" required="required" placeholder="Votre adresse e-mail *">
                                            </div>
                                            <div class="chp">
                                                <input type="text" class="form-control" name="telephone" id="tel" required="required" placeholder="Votre numéro de téléphone *">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="blc-btn">
                                        <input type="hidden" name="form_type" value="appointment">
                                        <button class="btn hvr-btn" id="confirm_devis">Prendre rendez vous</button>
                                    </div>
                                </div>
                                
                                
                                <a href="#pp-confirmation" id="topopup" class="fancybox" data-fancybox="" style="display: none;"></a>
                                <div class="pp-confirmation" id="pp-confirmation" style="display: none;">
                                    <div class="content">
                                        <h2>Confirmation</h2>
                                        <p>Votre demande de rendez-vous a été prise en compte </p>
                                    </div>
                                </div>
                            </div>-->

                        </div>
                    </form>
                    <!-- popup -->
                    <div id="popup_contact" class="white-popup mfp-hide">
                        <h3 class="tform_popup">Je souhaite être contacté</h3>
                        <?php echo do_shortcode( '[hf_form slug="right-contact"]' ); ?>
                    </div>
                    <!--# popup  -->
                </div>
            </section>

        </main>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBJwwWxqvXjx_NN1dq-DgGNhleyuZrSSyI&libraries=places&language=fr&callback=initAutocomplete"></script>
    
    
 <?php get_footer(); ?>

<?php 
/**
 * Template Name: Page actualite
 */
 ?>

<?php get_header(); ?>
    <main class="actualite">

        <section class="blcActus">
            <div class="container">

                <?php
                // the query
                $paged  = get_query_var('paged') ? get_query_var('paged') : 1;
                $args   = array('post_type'=>'post', 'post_status'=>'publish', 'posts_per_page'=>4, 'paged' => $paged);
                //$wpb_all_query = new WP_Query($args);
                query_posts($args); ?>

                <?php //if ( $wpb_all_query->have_posts() ) : ?>
                <?php if(have_posts()): ?>

                    <div class="lst-Actu clr">

                        <!-- the loop -->
                        <?php $i = 0;
                        //while ( $wpb_all_query->have_posts() ) :
                        while(have_posts()):
                            //$wpb_all_query->the_post();
                            the_post();
                            ?>

                            <div class="item wow <?= $i % 2 == 0 ? 'fadeInLeft' : 'fadeInRight' ?>" data-wow-delay="800ms">
                                <div class="content">
                                    <div class="img">
                                        <!-- <img src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/img-actus1.jpg" alt="Actualités"> -->
                                        <?php the_post_thumbnail('img-single') ?>
                                        <div class="hover">
                                            <div class="btn-h">
                                                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" class="btn hvr-btn">En savoir plus</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="txt">
                                        <a  href="<?php the_permalink() ?>" title="<?php the_title() ?>" class="titre"><?php the_title() ?></a>
                                        <span class="date"><?php the_date() ?></span>
                                    </div>
                                </div>
                            </div>

                        <?php $i++; endwhile; ?>
                        <!-- end of the loop -->

                    </div>

                    <div class="pagination">
                        <?php $pagin =  paginate_links(array('prev_next' => false, 'mid_size'  => 2, 'type' => 'array' )); ?>
                        <ul>
                            <!--<li class="prev"><a href="#"></a></li>-->
                            <li class="prev"><?php previous_posts_link( '' ); ?></li>
                            <?php
                            if ( is_array( $pagin) ):
                            foreach($pagin as $x): ?>
                            <li><?= $x ?></li>
                            <?php endforeach; 
                            endif;?>
                            <!--
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            -->
                            <!--<li class="next"><a href="#"></a></li>-->
                            <li class="next"><?php next_posts_link( '' ); ?></li>
                        </ul>
                    </div>

                    <?php wp_reset_postdata(); ?>

                <?php endif; ?>
            </div>
        </section>
    </main>
<?php get_footer(); ?>
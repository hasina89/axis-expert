<?php 
/**
* Template Name: Page accueil
*/
?>

<?php get_header(); ?>

<?php if ( have_posts() ) : ?>

    <?php while ( have_posts() ) : the_post(); ?>

            <section class="blcBienvenue">
                <div class="container clr">
                    <div class="videoWrapper videoWrapper169 js-videoWrapper video wow fadeInLeft" data-wow-delay="800ms">
                        <?php if ( get_field('video_lien_hp') ): ?>
                            <iframe src="<?= get_field('video_lien_hp') ?>" id="video" width="640" height="564" frameborder="0" allow="autoplay; fullscreen" allowfullscreen></iframe>
                        <?php else: ?>
                            <iframe src="https://player.vimeo.com/video/491549014?controls=0" id="video" width="640" height="564" frameborder="0" allow="fullscreen" allowfullscreen></iframe>
                        <?php endif; ?>

                        <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/bg-video.jpg);">Play</button>
                    </div>
                    <div class="texte wow fadeInRight" data-wow-delay="800ms">
                        <h2 class="titre"><?php the_field('titre_bvn'); ?></h2>
                        <div class="region"><?php the_field('region'); ?></div>
                        <p>
                            <?php the_field('text_de_bienvenue'); ?> 
                        </p>
                        <a href="<?php the_permalink(79);//29 avant:contact ?>" title="Prendre rendez-vous" class="btn hvr-btn">Prendre rendez-vous</a>
                    </div>

                </div>
            </section>
            <?php
                $args = array(
                    'post_type' => 'service',
                    'post_status' => 'publish',
                    'posts_per_page' => '6',
                    'order' => 'ASC'
                    );

                $service_loop = new WP_Query( $args );
                if ( $service_loop->have_posts() ) :
                $i = 0;
            ?>
            <section class="blcServices wow fadeIn" data-wow-delay="800ms">
                <div class="listServc clr" id="slideService">
                    <?php while ( $service_loop->have_posts() ) : $service_loop->the_post(); $i = $i+1; ?>
                    <div class="item <?php the_field('taille_element'); ?>" style="background-image: url(<?php the_post_thumbnail_url(); ?>);">
                        <div class="content">
                            <h4 class="s-titre"><?php the_title(); ?></h4>
                            <div class="hide">
                                <div class="table">
                                    <div class="texte">
                                        <h4 class="s-titre"><?php the_title(); ?></h4>
                                        <p>
                                            <?php the_excerpt(); ?>
                                        </p>  
                                        <a href="<?php the_permalink(); ?>" class="btn" title="En savoir plus">en savoir plus</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php switch ($i) { case 3: ?>
                    <div class="item item1 blcTitre">
                        <div class="content">
                            <h2 class="s-titre">Nos services</h2>
                        </div>
                    </div>
                    <?php break; } ?>
                    <?php
                        endwhile;
                        wp_reset_postdata();
                        endif;
                    ?>
                </div>             
            </section>
            <section class="blcCoord">
                <div class="container">
                    <div id="slideCoord" class="clr">
                        <div class="col horaire wow fadeIn" data-wow-delay="800ms">
                            <div class="content">
                                <div class="title"><?php the_field('jours','option'); ?>  </div>
                                <?php the_field('heures','option'); ?> 
                            </div>
                        </div>
                        <div class="col tel wow fadeIn" data-wow-delay="1200ms">
                            <div class="content">
                                <div class="title">telephone </div>
                                <?php $tel = get_field('telephone', 'option'); if( $tel ): ?>
                                    <a href="tel:<?php echo $tel['lien']; ?>" title="<?php echo $tel['afficher']; ?>"><?php echo $tel['afficher']; ?> </a>
                                <?php endif; ?> 
                            </div>
                        </div>
                        <div class="col mail wow fadeIn" data-wow-delay="1600ms">
                            <div class="content">
                                <div class="title">e-mail </div>
                                <a href="mailto:<?php the_field('mail', 'option'); ?>" title="<?php the_field('mail', 'option'); ?>"> <?php the_field('mail', 'option'); ?>   </a>
                            </div>
                        </div>
                        <div class="col rdv wow fadeIn" data-wow-delay="2000ms">
                            <div class="content">
                                <div class="title">prendre</div>
                                <a href="<?php the_permalink(79); ?>" title="Prendre rendez-vous">rendez-vous</a> 
                            </div>
                        </div> 
                    </div>           
                </div>
            </section>
            <section class="newsletter">
                <div class="container">
                    <h2 class="titre wow fadeInUp" data-wow-delay="800ms">Restez informé. </h2>
                    <h3 class="sousTitre wow fadeInUp" data-wow-delay="1200ms">Abonnez-vous à notre newsletter</h3>
                    <div class="formulaire-nl wow fadeInUp" data-wow-delay="1600ms">
                        <?php echo do_shortcode('[contact-form-7 id="312" title="Newsletter"]'); ?>                              
                    </div>   
                </div>             
            </section>
            <section class="blcPartenaire">
                <?php if( have_rows('listes_partenaire') ): ?>
                <div class="partenaire">
                    <div class="container">
                        <div class="listPartenaire clr" id="slidePartenaire">
                            <?php while ( have_rows('listes_partenaire') ) : the_row(); ?>
                            <div class="item">
                                <a href="<?php the_sub_field('lien_partenaire') ?>" target="_blank">
                                    <!--<img src="<?php //the_sub_field('logo_partenaire') ?>">-->
                                    <?php echo wp_get_attachment_image(get_sub_field('logo_partenaire'), 'partner-thumb'); ?>
                                </a>
                            </div>
                            <?php endwhile; ?>
                        </div>
                    </div>
                </div>
                <?php endif; ?>
                <div class="blcRs">
                    <div class="container">
                        <h2 class="titre  wow fadeInUp" data-wow-delay="800ms">Suivez-nous sur <span>les réseaux sociaux</span></h2>
                        <div class=" wow fadeInUp" data-wow-delay="1200ms"> 
                            <p>
                                <?php the_field('text_rs'); ?>
                            </p>
                        </div>
                        <ul class="listRs">
                            <li class="wow fadeInUp" data-wow-delay="800ms">
                                <a href="<?php echo get_field('facebook', 'option') ?? "#" ?>" class="fb" title="Suivez-nous sur Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icone-fb.svg"></a>
                            </li><!-- 
                            <li class="wow fadeInUp" data-wow-delay="1200ms">
                                <a href="#" class="twitter" title="Suivez-nous sur Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icone-twitter.svg"></a>
                            </li> -->
                            <li class="wow fadeInUp" data-wow-delay="1600ms">
                                <a href="<?php echo get_field('linkedin', 'option') ?? "#" ?>" class="ln" title="Suivez-nous sur Linkedin" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/images/icone-ln.svg"></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </section>
            <section class="blcTestimonial">
                <div class="container">
                    <h2 class="titre">Ce que nos clients disent de nous</h2>
                    <?php 
                        $args = array(
                            'post_type' => 'avis_client',
                            'post_status' => 'publish',
                            'posts_per_page' => '-1',
                            'order' => 'ASC'
                            );

                        $avisClient_loop = new WP_Query( $args );
                        if ( $avisClient_loop->have_posts() ) :
                    ?>
                    <div class="listTestimonial" id="slideTestimonial">
                        <?php while ( $avisClient_loop->have_posts() ) : $avisClient_loop->the_post(); ?> 
                        <div class="item">
                            <div class="text">
                                <p>
                                    <?php the_content(); ?> 
                                </p>
                            </div>
                            <div class="author">
                                <?php the_title(); ?> <span><?php the_field('societe') ?></span>
                            </div>
                            <?php $note = get_field('appreciation'); ?>
                            <ul class="etoile">
                                <?php switch ($note) {  case 1: ?>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <?php break; case 2: ?>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <?php break; case 3: ?>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <?php break; case 4: ?>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial-on.png"></li>
                                <?php break; case 5: ?>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <li><img src="<?php echo get_template_directory_uri(); ?>/images/dot-testimonial.png"></li>
                                <?php break; } ?>
                            </ul>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <?php
                        wp_reset_postdata();
                        endif;
                    ?>
                </div>
            </section>

        <?php endwhile; ?>

    <?php endif; ?>

    <?php get_footer(); ?>
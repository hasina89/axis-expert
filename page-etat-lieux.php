<?php 
/**
 * Template Name: Page etat lieu
 */
 ?>

<?php get_header(); ?>

<main>
	<?php while ( have_posts() ) : the_post();?>
		<section class="blcBienvenue blcTextPage">
			<div class="container clr">
				<div class="texte wow fadeInLeft" data-wow-delay="800ms">
					<h2 class="titrePage"><?php the_field('titre'); ?></h2>
					<p>
						<?php the_field('text'); ?> 
					</p>
					<a href="<?php the_permalink(29); ?>" title="Prendre rendez-vous" class="btn hvr-btn">besoin d’une offre de prix</a>
				</div>
				<div class="video wow fadeInRight" data-wow-delay="800ms">
					<img src="<?php the_field('images'); ?>">
          		</div>
			</div>         
		</section>
		<section class="blcPartenaire etatLieu">
            <?php if( have_rows('listes_partenaire') ): ?>
            <div class="partenaire">
                <div class="container">
                    <div class="listPartenaire clr" id="slidePartenaire">
                        <?php while ( have_rows('listes_partenaire') ) : the_row(); ?>
                        <div class="item">
                            <a href="<?php the_sub_field('lien_partenaire') ?>" target="_blank"><img src="<?php the_sub_field('logo_partenaire') ?>"></a>
                        </div>
                        <?php endwhile; ?>
                    </div>
                    <div class="center">
                    	<strong>Certificat de bonne exécution de travaux</strong> pour toutes entreprises avec qui nous travaillons
                    </div>
                </div>
            </div>
            <?php endif; ?>
        </section>
        <section class="blcBienvenue blcTextPage blcTextPage2">
        	<div class="container">
        		<div class="video wow fadeInLeft" data-wow-delay="800ms">
        			<img src="<?php the_field('image_p2'); ?>">
        		</div>
        		<div class="text wow fadeInRight" data-wow-delay="800ms">
        			<h2 class="titrePage"><?php the_field('titre_p2'); ?></h2>
        			<?php the_field('text_p2'); ?>
        		</div> 
        	</div>         
        </section>
        <section class="blcCoord">
        	<div class="container">
        		<div id="slideCoord" class="clr">
        			<div class="col horaire wow fadeIn" data-wow-delay="800ms">
        				<div class="content">
        					<div class="title"><?php the_field('jours','option'); ?>  </div>
        					<?php the_field('heures','option'); ?> 
        				</div>
        			</div>
        			<div class="col tel wow fadeIn" data-wow-delay="1200ms">
        				<div class="content">
        					<div class="title">telephone </div>
        					<?php $tel = get_field('telephone', 'option'); if( $tel ): ?>
	        					<a href="tel:<?php echo $tel['lien']; ?>" title="<?php echo $tel['afficher']; ?>"><?php echo $tel['afficher']; ?> </a>
	        				<?php endif; ?> 
	        			</div>
	        		</div>
	        		<div class="col mail wow fadeIn" data-wow-delay="1600ms">
	        			<div class="content">
	        				<div class="title">e-mail </div>
	        				<a href="mailto:<?php the_field('mail', 'option'); ?>" title="<?php the_field('mail', 'option'); ?>"> <?php the_field('mail', 'option'); ?>   </a>
	        			</div>
	        		</div>
	        		<div class="col rdv wow fadeIn" data-wow-delay="2000ms">
	        			<div class="content">
	        				<div class="title">prendre</div>
	        				<a href="<?php the_permalink(79); ?>" title="Prendre rendez-vous">rendez-vous</a> 
	        			</div>
	        		</div> 
	        	</div>           
	        </div>
	    </section>


	<?php endwhile; ?>
</main>

<?php get_footer(); ?>
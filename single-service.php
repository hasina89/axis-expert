<?php get_header(); ?>

<main>
	<?php while ( have_posts() ) : the_post();?>
    <section class="blcBienvenue blcTextPage">
        <div class="container clr">
          <div class="texte wow fadeInLeft" data-wow-delay="800ms">
               <h2 class="titrePage"><?php the_field('titre'); ?></h2>
               <p>
                   <?php the_field('text'); ?> 
               </p>
               <a href="<?php the_permalink(79); ?>" title="Prendre rendez-vous" class="btn hvr-btn">Prendre rendez-vous</a>
           </div>
           <?php if ( $post->post_name == "reception-provisoire" ){ ?>
            <div class="video wow fadeInRight" data-wow-delay="800ms">
               <img src="<?php echo get_template_directory_uri(); ?>/images/reception-provisoire.jpg" alt="">
            </div>
          <?php }else{ 
            if ( get_field('lien_video' ) ):
            ?>
            <div class="video wow fadeInRight" data-wow-delay="800ms">
              <iframe src="<?= get_field('lien_video' ) ?>" id="video" width="640" height="360" frameborder="0" allow=" fullscreen" allowfullscreen></iframe>
              <button class="videoPoster js-videoPoster" style="background-image:url(<?php echo get_template_directory_uri(); ?>/images/bg-video.jpg);">Play</button>
            </div>
          <?php endif; } ?>
        </div>
    </section>
    <section class="blcAvantage">
        <div class="container">
            <h2 class="titrePage wow fadeInUp" data-wow-delay="800ms"><?php the_field('titre_av'); ?></h2>
            <?php if( have_rows('chiffre') ): $i=0; ?>
            <div class="blcChiffre clr wow fadeInUp" data-wow-delay="1200ms">
				<?php while ( have_rows('chiffre') ) : the_row();$i++; ?>
             	<div class="item item<?php echo $i; ?>">
                    <div class="nombre"><?php if( get_sub_field('indice') == 'enable_sidebar' ) {?>+<?php } ?><span class="counter"><?php the_sub_field('nombre'); ?></span></div>
                    <div class="title"><?php the_sub_field('titre'); ?></div>
                </div>
                <?php endwhile; ?>
            </div>
            <?php endif; ?>
        </div>
    </section>
    <section class="blcBienvenue blcTextPage blcTextPage2">
        <div class="container">
          <div class="video wow fadeInLeft" data-wow-delay="800ms">
             <img src="<?php the_field('image'); ?>">
          </div>
           <div class="texte wow fadeInRight" data-wow-delay="800ms">
               <h2 class="titrePage"><?php the_field('titre_p2'); ?></h2>
               <?php the_field('text_p2'); ?>
           </div> 
        </div>         
    </section>
   <section class="blcCoord">
        <div class="container">
            <div id="slideCoord" class="clr">
                <div class="col horaire wow fadeIn" data-wow-delay="800ms">
                    <div class="content">
                        <div class="title"><?php the_field('jours','option'); ?>  </div>
                        <?php the_field('heures','option'); ?>
                    </div>
                </div>
                <div class="col tel wow fadeIn" data-wow-delay="1200ms">
                    <div class="content">
                        <div class="title">telephone </div>
                        <?php $tel = get_field('telephone', 'option'); if( $tel ): ?>
                            <a href="tel:<?php echo $tel['lien']; ?>" title="<?php echo $tel['afficher']; ?>"><?php echo $tel['afficher']; ?> </a>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col mail wow fadeIn" data-wow-delay="1600ms">
                    <div class="content">
                        <div class="title">e-mail </div>
                        <a href="mailto:<?php the_field('mail', 'option'); ?>" title="<?php the_field('mail', 'option'); ?>"> <?php the_field('mail', 'option'); ?>   </a>
                    </div>
                </div>
                <div class="col rdv wow fadeIn" data-wow-delay="2000ms">
                    <div class="content">
                        <div class="title">prendre</div>
                        <a href="<?php the_permalink(79); ?>" title="Prendre rendez-vous">rendez-vous</a> 
                    </div>
                </div> 
            </div>           
        </div>
    </section>
	<?php endwhile; ?>
</main>
<?php get_footer(); ?>
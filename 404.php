<?php get_header(); ?>
	<div class="container">
		<section class="http-error">
			<div class="justify-content-center py-3">
				<div class="text-center">
					<div class="http-error-main">
						<h2>404!</h2>
						<p>Page non trouver!</p>
					</div>
				</div>
			</div>
		</section>
	</div>
<?php get_footer(); ?>

<?php 
   $par_partie = '';
   if( $type_mission == '399' || $type_mission == '401' )
      $par_partie = ' par partie';
 ?>
<!doctype html>
<html xmlns="https://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <title>
      </title>
      <!--[if !mso]><!-- -->
      <meta https-equiv="X-UA-Compatible" content="IE=edge">
      <!--<![endif]-->
      <meta https-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <style type="text/css">
         #outlook a {
         padding: 0;
         }
         body {
         margin: 0;
         padding: 0;
         -webkit-text-size-adjust: 100%;
         -ms-text-size-adjust: 100%;
         }
         table,
         td {
         border-collapse: collapse;
         mso-table-lspace: 0pt;
         mso-table-rspace: 0pt;
         }
         img {
         border: 0;
         height: auto;
         line-height: 100%;
         outline: none;
         text-decoration: none;
         -ms-interpolation-mode: bicubic;
         }
         p {
         display: block;
         margin: 13px 0;
         }
      </style>
      <!--[if mso]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <!--[if lte mso 11]>
      <style type="text/css">
         .mj-outlook-group-fix { width:100% !important; }
      </style>
      <![endif]-->
      <style type="text/css">
         @media only screen and (min-width:480px) {
         .mj-column-per-10 {
         width: 10% !important;
         max-width: 10%;
         }
         .mj-column-per-90 {
         width: 90% !important;
         max-width: 90%;
         }
         .mj-column-per-100 {
         width: 100% !important;
         max-width: 100%;
         }
         .mj-column-per-25 {
         width: 25% !important;
         max-width: 25%;
         }
         }
      </style>
      <style type="text/css">
         @media only screen and (max-width:480px) {
         table.mj-full-width-mobile {
         width: 100% !important;
         }
         td.mj-full-width-mobile {
         width: auto !important;
         }
         }
      </style>
      <style type="text/css">
         a {
         text-decoration: none !important;
         }
         @media only screen and (max-width:560px) {
         .header {
         padding: 30px 20px;
         }
         .header>table>tbody>tr>td {
         padding: 0 !important;
         }
         .logo {
         padding-bottom: 20px !important;
         }
         .logo>table>tbody>tr>td>table>tbody>tr>td>table {
         margin: 0 auto;
         }
         .slogan>table>tbody>tr>td>table>tbody>tr>td>div {
         font-size: 16px !important;
         line-height: 20px !important;
         text-align: center !important;
         }
         .coor>table>tbody>tr>td {
         padding: 25px 20px !important;
         }
         .width {
         width: 120px !important;
         }
         .main>table>tbody>tr>td {
         padding: 0 0 0 !important;
         }
         .tableau>table>tbody>tr>td>table>tbody>tr>td>table>tbody>tr>td {
         width: 50% !important;
         vertical-align: top;
         }
         .mj-column-per-25{ width: 50%!important;padding-bottom: 15px!important }
         .blcBanner>div>table>tbody>tr>td{ padding-bottom: 35px!important; }
         .col-titre>table>tbody>tr>td {
            padding: 25px 20px 0!important;
         }
         .salutation2>table>tbody>tr>td{ padding:25px 20px!important; }
         }
      </style>
   </head>
   <body style="background-color:#ffffff;">
      <div style="background-color:#ffffff;">
         <!--[if mso | IE]>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="header-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="header" style="background:#6eba32;background-color:#6eba32;margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#6eba32;background-color:#6eba32;width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:25px 40px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="logo-outlook" style="vertical-align:top;width:57px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-10 mj-outlook-group-fix logo" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="left" style="font-size:0px;padding:0;word-break:break-word;">
                                                                  <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                     <tbody>
                                                                        <tr>
                                                                           <td style="width:46px;">
                                                                              <a href="http://axis-expert.maki-group.mg/" target="_blank">
                                                                              <img alt="Axis" height="62" src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/logo-mail.png" style="border:0;display:block;outline:none;text-decoration:none;height:62px;width:100%;font-size:13px;" width="46" />
                                                                              </a>
                                                                           </td>
                                                                        </tr>
                                                                     </tbody>
                                                                  </table>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                       <td
                                          class="slogan-outlook" style="vertical-align:top;width:513px;"
                                          >
                                          <![endif]-->
                                          <div class="mj-column-per-90 mj-outlook-group-fix slogan" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                             <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                <tbody>
                                                   <tr>
                                                      <td style="vertical-align:top;padding:0px;">
                                                         <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                            <tr>
                                                               <td align="center" style="font-size:0px;padding:0px;padding-left:20px;word-break:break-word;">
                                                                  <div style="font-family:Arial;font-size:24px;font-weight:700;letter-spacing:.6px;line-height:60px;text-align:center;text-transform:uppercase;color:#ffffff;">
                                                                     <font color="#fff" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                     <b><?= stripslashes(esc_html($subject)) ?></b>
                                                                     </font>
                                                                  </div>
                                                               </td>
                                                            </tr>
                                                         </table>
                                                      </td>
                                                   </tr>
                                                </tbody>
                                             </table>
                                          </div>
                                          <!--[if mso | IE]>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="main-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="main" style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:45px 0;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="" width="650px"
                                          >
                                          <table
                                             align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
                                             >
                                             <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                   <![endif]-->
                                                   <div style="margin:0px auto;max-width:650px;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                         <tbody>
                                                            <tr>
                                                               <td style="direction:ltr;font-size:0px;padding:0 0 35px;text-align:center;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td
                                                                           class="" style="vertical-align:top;width:650px;"
                                                                           >
                                                                           <![endif]-->
                                                                           <div class="mj-column-per-100 mj-outlook-group-fix col-titre" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                              <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="vertical-align:top;padding:0 ;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                             <tr>
                                                                                                <td align="left" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                   <table cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#000000;font-family:Arial;font-size:13px;line-height:22px;table-layout:auto;width:100%;border:none;">
                                                                                                      <tr>
                                                                                                         <td style="width:24px;" valign="middle">
                                                                                                            <img src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/puce-mail.png" width="24" height="auto" style="width: 100%; height: auto; display: block; border: 0; line-height: 100%; outline: none; text-decoration: none;" alt="" />
                                                                                                         </td>
                                                                                                         <td valign="top" style="padding-left:15px; font-size:15px; line-height:24px; color:#1a1a1a;">
                                                                                                            <b><?= get_field('dd_titre_paragraphe','option') ?></b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                <td align="left" style="font-size:0px;padding:0px;padding-top:15px;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:12px;font-weight:400;letter-spacing:.1px;line-height:22px;text-align:left;color:#929292;">
                                                                                                      <font color="#929292" font-size="12px" line-height="22px"><?= get_field('dd_texte_daccueil','option') ?></font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                   <!--[if mso | IE]>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td
                                          class="coor-outlook" width="650px"
                                          >
                                          <table
                                             align="center" border="0" cellpadding="0" cellspacing="0" class="coor-outlook" style="width:650px;" width="650"
                                             >
                                             <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                   <![endif]-->
                                                   <div class="coor" style="background:#f2f2f2;background-color:#f2f2f2;margin:0px auto;max-width:650px;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#f2f2f2;background-color:#f2f2f2;width:100%;">
                                                         <tbody>
                                                            <tr>
                                                               <td style="direction:ltr;font-size:0px;padding:35px 40px;text-align:center;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td
                                                                           class="tableau-outlook" style="vertical-align:top;width:490px;"
                                                                           >
                                                                           <![endif]-->
                                                                           <div class="mj-column-per-100 mj-outlook-group-fix tableau" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                              <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="vertical-align:top;padding:0px;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                             <tr>
                                                                                                <td align="left" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                   <table cellpadding="0" cellspacing="0" width="100%" border="0" style="color:#000000;font-family:Arial;font-size:14px;line-height:20px;table-layout:auto;width:100%;border:none;">
                                                                                                      <tr>
                                                                                                         <td style="width:165px;padding-bottom:5px;" valign="middle"> Nom : </td>
                                                                                                         <td style="padding-bottom:5px;" valign="middle">
                                                                                                            <b><?= $nom; ?></b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td style="width:165px;padding-bottom:5px;" valign="middle"> Email : </td>
                                                                                                         <td style="padding-bottom:5px;" valign="middle">
                                                                                                            <b><?= $email; ?></b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td style="width:165px;padding-bottom:15px;" valign="middle"> Adresse : </td>
                                                                                                         <td style="padding-bottom:15px;" valign="middle">
                                                                                                            <?php 
                                                                                                            if ( $numeroAppart != '' ):
                                                                                                               $adresse = $numeroAppart.' '.$adresse;
                                                                                                            endif; ?>
                                                                                                            <b> <?= $adresse; ?> </b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td style="width:165px;padding-bottom:15px;" valign="middle"> Commentaire : </td>
                                                                                                         <td style="padding-bottom:15px;" valign="middle">
                                                                                                            <b> <?= stripslashes(esc_html($comment)); ?> </b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td style="width:165px;padding-bottom:5px;" valign="middle"> Type de mission : </td>
                                                                                                         <td style="padding-bottom:5px;" valign="middle">
                                                                                                            <b> <?php echo $tm ?> </b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td style="width:165px;padding-bottom:5px;" valign="middle"> Type d&#8217;immeuble : </td>
                                                                                                         <td style="padding-bottom:5px;" valign="middle">
                                                                                                            <b><?= get_post_info($type_immeuble); ?></b>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                         <?php if ( !$surface_only && !$meuble_only): ?>
                                                                                                            <tr>
                                                                                                               <td style="width:165px;" valign="middle"> Chambre : </td>
                                                                                                               <td valign="middle">
                                                                                                                  <b><?= $nb_chambre; ?></b>
                                                                                                               </td>
                                                                                                            </tr>
                                                                                                            <tr>
                                                                                                               <td style="width:165px;" valign="middle"> Salle de bain : </td>
                                                                                                               <td valign="middle">
                                                                                                                  <b><?= $nb_salle_bain; ?></b>
                                                                                                               </td>
                                                                                                            </tr>
                                                                                                         <?php endif; ?>
                                                                                                         <?php if ( !$meuble_only ): ?>
                                                                                                            <?php if ( $surface ): ?>
                                                                                                                <tr>
                                                                                                                   <td style="width:165px;padding-bottom:15px" valign="middle"> Surface : </td>
                                                                                                                   <td valign="middle" style="padding-bottom:15px">
                                                                                                                      <b><?= $surface; ?> m<sup>2</sup></b>
                                                                                                                   </td>
                                                                                                                </tr>
                                                                                                            <?php endif; ?>
                                                                                                         <?php endif; ?>
                                                                                                         <?php if( $avec_options == 'oui' || $type_immeuble == '406' || $type_immeuble == '428' || $type_immeuble == '443' ): ?>
                                                                                                            <tr>
                                                                                                               <td style="width:165px;" valign="top"> Autres options : </td>
                                                                                                               <td valign="top">
                                                                                                                  <?php
                                                                                                                  $url_option = '';
                                                                                                                   foreach ($all_options as $k => $all_option) {
                                                                                                                        $url_option .= $all_option['name'];
                                                                                                                        if ( $k + 1 != count( $all_options ) ) $url_option .= '|';
                                                                                                                       echo '<b>'.$all_option['name'].'</b><br>';
                                                                                                                   }
                                                                                                                   ?></b><br>
                                                                                                               </td>
                                                                                                            </tr>
                                                                                                         <?php endif; ?>
                                                                                                      <tr>
                                                                                                         <td style="height:25px" height="25" valign="middle"></td>
                                                                                                      </tr>
                                                                                                      <?php if ( $reduction > 0 ): ?>
                                                                                                         <tr>
                                                                                                            <td style="width:165px;" valign="middle">
                                                                                                               Honoraire normale de la mission :
                                                                                                            </td>
                                                                                                               <td valign="middle"><b> <?= $grand_total.'€'.$par_partie ?></b></td>
                                                                                                         </tr>
                                                                                                         <tr>
                                                                                                            <td style="width:165px;padding-bottom:5px;" valign="middle"> Votre remise : </td>
                                                                                                            <td style="padding-bottom:5px;" valign="middle">
                                                                                                               <b><?= $reduction ?>%</b>
                                                                                                            </td>
                                                                                                         </tr>
                                                                                                         <tr>
                                                                                                            <td style="width:165px;" valign="middle">
                                                                                                               Honoraire remisé de la mission :
                                                                                                            </td>
                                                                                                               <?php $prix_reduit = $grand_total - $reduit; ?>
                                                                                                               <td valign="middle"><b> <?=  $prix_reduit.'€'.$par_partie ?></b></td>
                                                                                                         </tr>
                                                                                                         <tr>
                                                                                                            <td style="height:25px" height="25" valign="middle"></td>
                                                                                                         </tr>
                                                                                                      <?php else: ?>
                                                                                                      <tr>
                                                                                                         <td style="width:165px;" valign="middle">
                                                                                                            Honoraire de la mission :
                                                                                                         </td>
                                                                                                            <td valign="middle"><b> <?= $grand_total.'€'.$par_partie ?></b></td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td style="height:25px" height="25" valign="middle"></td>
                                                                                                      </tr>
                                                                                                      <?php endif; ?>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                     <tr>
                                                                                        <td align="center" class="sary" style="font-size:0px;padding:0px;padding-top:20px;word-break:break-word;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                                            <tbody>
                                                                                              <tr>
                                                                                                <td style="width:233px;">
                                                                                                  <a href="http://axis-expert.maki-group.mg/recapitulatif-de-votre-devis/?nom=<?= urlencode($nom) ?>&email=<?= $email ?>&adresse=<?= urlencode($adresse) ?>&rem=<?= $reduction ?>&coms=<?= urlencode(stripslashes(esc_html($comment)) ) ?>&mission=<?= urlencode($tm) ?>&immeuble=<?= urlencode(get_post_info($type_immeuble)) ?>&nb_chambre=<?= $nb_chambre ?>&sdb=<?= $nb_salle_bain ?>&surface=<?= $surface ?>&option=<?= urlencode($url_option) ?>&honoraire=<?= urlencode($grand_total) ?>" target="_blank" title="finaliser">
                                                                                                    <img height="auto" src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/btn-devis.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="233" />
                                                                                                  </a>
                                                                                                </td>
                                                                                              </tr>
                                                                                            </tbody>
                                                                                          </table>
                                                                                        </td>
                                                                                      </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                   <!--[if mso | IE]>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="-outlook blcBanner-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <v:rect  style="width:650px;" xmlns:v="urn:schemas-microsoft-com:vml" fill="true" stroke="false">
                     <v:fill  origin="0, -0.5" position="0, -0.5" src="http://axis-expert.maki-group.mg/wp-content/uploads/2020/11/bg-banner.jpg" type="frame" size="1,1" aspect="atleast" />
                     <v:textbox style="mso-fit-shape-to-text:true" inset="0,0,0,0">
                        <![endif]-->
                        <div class=" blcBanner" style="background:url(http://axis-expert.maki-group.mg/wp-content/uploads/2020/11/bg-banner.jpg) center top / cover no-repeat;background-position:center top;background-repeat:no-repeat;background-size:cover;margin:0px auto;max-width:650px;">
                           <div style="line-height:0;font-size:0;">
                              <table align="center" background="http://axis-expert.maki-group.mg/wp-content/uploads/2020/11/bg-banner.jpg" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:url(http://axis-expert.maki-group.mg/wp-content/uploads/2020/11/bg-banner.jpg) center top / cover no-repeat;background-position:center top;background-repeat:no-repeat;background-size:cover;width:100%;">
                                 <tbody>
                                    <tr>
                                       <td style="direction:ltr;font-size:0px;padding:40px 20px 50px;text-align:center;">
                                          <!--[if mso | IE]>
                                          <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                             <tr>
                                                <td
                                                   class="" width="650px"
                                                   >
                                                   <table
                                                      align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:610px;" width="610"
                                                      >
                                                      <tr>
                                                         <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                            <![endif]-->
                                                            <div style="margin:0px auto;max-width:610px;">
                                                               <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="direction:ltr;font-size:0px;padding:0px;text-align:center;">
                                                                           <!--[if mso | IE]>
                                                                           <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                              <tr>
                                                                                 <td
                                                                                    align="center" class="" style=""
                                                                                    >
                                                                                    <![endif]-->
                                                                                    <div style="font-family:Arial;font-size:36px;font-weight:700;letter-spacing:.6px;line-height:45px;text-align:center;color:#ffffff;">
                                                                                       <font color="#fff" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                       <b>Nos avantages</b>
                                                                                       </font>
                                                                                    </div>
                                                                                    <!--[if mso | IE]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </div>
                                                            <!--[if mso | IE]>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                             <tr>
                                                <td
                                                   class="" width="650px"
                                                   >
                                                   <table
                                                      align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:610px;" width="610"
                                                      >
                                                      <tr>
                                                         <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                            <![endif]-->
                                                            <div style="margin:0px auto;max-width:610px;">
                                                               <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                                  <tbody>
                                                                     <tr>
                                                                        <td style="direction:ltr;font-size:0px;padding:0px;padding-top:40px ;text-align:center;">
                                                                           <!--[if mso | IE]>
                                                                           <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                              <tr>
                                                                                 <td
                                                                                    class="" style="vertical-align:top;width:152.5px;"
                                                                                    >
                                                                                    <![endif]-->
                                                                                    <div class="mj-column-per-25 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                                       <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td style="vertical-align:top;padding:0px;">
                                                                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:30px;font-weight:700;letter-spacing:.6px;line-height:32px;text-align:center;color:#ffffff;">
                                                                                                               <font color="#ffffff" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>+3</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:18px;font-weight:700;letter-spacing:.6px;line-height:25px;text-align:center;color:#6eba32;">
                                                                                                               <font color="#6eba32" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>Secrétaires</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </div>
                                                                                    <!--[if mso | IE]>
                                                                                 </td>
                                                                                 <td
                                                                                    class="" style="vertical-align:top;width:152.5px;"
                                                                                    >
                                                                                    <![endif]-->
                                                                                    <div class="mj-column-per-25 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                                       <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td style="vertical-align:top;padding:0px;">
                                                                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:30px;font-weight:700;letter-spacing:.6px;line-height:32px;text-align:center;color:#ffffff;">
                                                                                                               <font color="#ffffff" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>100</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:18px;font-weight:700;letter-spacing:.6px;line-height:25px;text-align:center;color:#6eba32;">
                                                                                                               <font color="#6eba32" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>numérique !</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </div>
                                                                                    <!--[if mso | IE]>
                                                                                 </td>
                                                                                 <td
                                                                                    class="" style="vertical-align:top;width:152.5px;"
                                                                                    >
                                                                                    <![endif]-->
                                                                                    <div class="mj-column-per-25 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                                       <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td style="vertical-align:top;padding:0px;">
                                                                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:30px;font-weight:700;letter-spacing:.6px;line-height:32px;text-align:center;color:#ffffff;">
                                                                                                               <font color="#ffffff" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>+6</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:18px;font-weight:700;letter-spacing:.6px;line-height:25px;text-align:center;color:#6eba32;">
                                                                                                               <font color="#6eba32" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>Experts</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </div>
                                                                                    <!--[if mso | IE]>
                                                                                 </td>
                                                                                 <td
                                                                                    class="" style="vertical-align:top;width:152.5px;"
                                                                                    >
                                                                                    <![endif]-->
                                                                                    <div class="mj-column-per-25 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                                       <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                          <tbody>
                                                                                             <tr>
                                                                                                <td style="vertical-align:top;padding:0px;">
                                                                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:30px;font-weight:700;letter-spacing:.6px;line-height:32px;text-align:center;color:#ffffff;">
                                                                                                               <font color="#ffffff" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>+3000</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                      <tr>
                                                                                                         <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                            <div style="font-family:Arial;font-size:18px;font-weight:700;letter-spacing:.6px;line-height:25px;text-align:center;color:#6eba32;">
                                                                                                               <font color="#6eba32" font-size="24px" line-height="60px" letter-spacing=".6px">
                                                                                                               <b>Dossiers / an</b>
                                                                                                               </font>
                                                                                                            </div>
                                                                                                         </td>
                                                                                                      </tr>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </tbody>
                                                                                       </table>
                                                                                    </div>
                                                                                    <!--[if mso | IE]>
                                                                                 </td>
                                                                              </tr>
                                                                           </table>
                                                                           <![endif]-->
                                                                        </td>
                                                                     </tr>
                                                                  </tbody>
                                                               </table>
                                                            </div>
                                                            <!--[if mso | IE]>
                                                         </td>
                                                      </tr>
                                                   </table>
                                                </td>
                                             </tr>
                                          </table>
                                          <![endif]-->
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <!--[if mso | IE]>
                     </v:textbox>
                  </v:rect>
               </td>
            </tr>
         </table>
         <table
            align="center" border="0" cellpadding="0" cellspacing="0" class="main-outlook" style="width:650px;" width="650"
            >
            <tr>
               <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                  <![endif]-->
                  <div class="main" style="margin:0px auto;max-width:650px;">
                     <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                        <tbody>
                           <tr>
                              <td style="direction:ltr;font-size:0px;padding:0 0 40px;text-align:center;">
                                 <!--[if mso | IE]>
                                 <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                    <tr>
                                       <td
                                          class="salutation-outlook" width="650px"
                                          >
                                          <table
                                             align="center" border="0" cellpadding="0" cellspacing="0" class="salutation-outlook" style="width:650px;" width="650"
                                             >
                                             <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                   <![endif]-->
                                                   <div class="salutation salutation2" style="margin:0px auto;max-width:650px;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                         <tbody>
                                                            <tr>
                                                               <td style="direction:ltr;font-size:0px;padding:35px 0 50px;text-align:center;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td
                                                                           class="" style="vertical-align:top;width:650px;"
                                                                           >
                                                                           <![endif]-->
                                                                           <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                              <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="vertical-align:top;padding:0px;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:15px 0 0;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:14px;font-weight:400;letter-spacing:.1px;line-height:24px;text-align:center;color:#1a1a1a;">
                                                                                                      <font color="#1a1a1a" font-size="14px" line-height="24px"><?= get_field('dd_remerciement','option') ?></font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:15px 0 0;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:14px;font-weight:400;letter-spacing:.1px;line-height:24px;text-align:center;color:#1a1a1a;">
                                                                                                      <font color="#1a1a1a" font-size="14px" line-height="24px"><?= get_field('dd_cgu','option') ?></font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                   <!--[if mso | IE]>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td
                                          class="" width="650px"
                                          >
                                          <table
                                             align="center" border="0" cellpadding="0" cellspacing="0" class="" style="width:650px;" width="650"
                                             >
                                             <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                   <![endif]-->
                                                   <div style="background:#6eba32;background-color:#6eba32;margin:0px auto;max-width:650px;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="background:#6eba32;background-color:#6eba32;width:100%;">
                                                         <tbody>
                                                            <tr>
                                                               <td style="direction:ltr;font-size:0px;padding:35px 20px;text-align:center;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td
                                                                           class="" style="vertical-align:top;width:530px;"
                                                                           >
                                                                           <![endif]-->
                                                                           <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                              <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="vertical-align:top;padding:0px;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:0px;word-break:break-word;">
                                                                                                   <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;border-spacing:0px;">
                                                                                                      <tbody>
                                                                                                         <tr>
                                                                                                            <td style="width:82px;">
                                                                                                               <a href="#" target="_blank">
                                                                                                               <img height="auto" src="http://axis-expert.maki-group.mg/wp-content//themes/axis/images/logo-f.png" style="border:0;display:block;outline:none;text-decoration:none;height:auto;width:100%;font-size:13px;" width="82" alt="" />
                                                                                                               </a>
                                                                                                            </td>
                                                                                                         </tr>
                                                                                                      </tbody>
                                                                                                   </table>
                                                                                                </td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:5px 0 0;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:14px;font-weight:400;letter-spacing:.1px;line-height:24px;text-align:center;color:#376b37;">
                                                                                                      <font color="#376b37" font-size="14px" line-height="24px"> <?= get_field('localisation','option') ?> </font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:25px 0 0;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:14px;font-weight:400;letter-spacing:.1px;line-height:24px;text-align:center;color:#376b37;">
                                                                                                      <font color="#376b37" font-size="14px" line-height="24px"> <?= get_field('adresse','option') ?></font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:25px 0 0;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:14px;font-weight:400;letter-spacing:.1px;line-height:24px;text-align:center;color:#376b37;"><a href="tel:028809090">
                                                                                                      <font color="#376b37" font-size="14px" line-height="24px"><u>02 / 880 90 90</u></font>
                                                                                                      </a>
                                                                                                      <font color="#ffffff">//</font><a href="mailto:info@axis-experts.be">
                                                                                                      <font color="#376b37" font-size="14px" line-height="24px"><u><?= get_field('mail','option') ?></u></font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                   <!--[if mso | IE]>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td
                                          class="salutation-outlook" width="650px"
                                          >
                                          <table
                                             align="center" border="0" cellpadding="0" cellspacing="0" class="salutation-outlook" style="width:650px;" width="650"
                                             >
                                             <tr>
                                                <td style="line-height:0px;font-size:0px;mso-line-height-rule:exactly;">
                                                   <![endif]-->
                                                   <div class="salutation" style="margin:0px auto;max-width:650px;">
                                                      <table align="center" border="0" cellpadding="0" cellspacing="0" role="presentation" style="width:100%;">
                                                         <tbody>
                                                            <tr>
                                                               <td style="direction:ltr;font-size:0px;padding:35px 0 50px;text-align:center;">
                                                                  <!--[if mso | IE]>
                                                                  <table role="presentation" border="0" cellpadding="0" cellspacing="0">
                                                                     <tr>
                                                                        <td
                                                                           class="" style="vertical-align:top;width:650px;"
                                                                           >
                                                                           <![endif]-->
                                                                           <div class="mj-column-per-100 mj-outlook-group-fix" style="font-size:0px;text-align:left;direction:ltr;display:inline-block;vertical-align:top;width:100%;">
                                                                              <table border="0" cellpadding="0" cellspacing="0" role="presentation" width="100%">
                                                                                 <tbody>
                                                                                    <tr>
                                                                                       <td style="vertical-align:top;padding:0px;">
                                                                                          <table border="0" cellpadding="0" cellspacing="0" role="presentation" style="" width="100%">
                                                                                             <tr>
                                                                                                <td align="center" style="font-size:0px;padding:0;word-break:break-word;">
                                                                                                   <div style="font-family:Arial;font-size:14px;font-weight:400;letter-spacing:.1px;line-height:24px;text-align:center;color:#1a1a1a;">
                                                                                                      <font color="#1a1a1a" font-size="12px" line-height="24px">
                                                                                                         <?= get_field('dd_text_perso','option'); ?>
                                                                                                      </font>
                                                                                                   </div>
                                                                                                </td>
                                                                                             </tr>
                                                                                          </table>
                                                                                       </td>
                                                                                    </tr>
                                                                                 </tbody>
                                                                              </table>
                                                                           </div>
                                                                           <!--[if mso | IE]>
                                                                        </td>
                                                                     </tr>
                                                                  </table>
                                                                  <![endif]-->
                                                               </td>
                                                            </tr>
                                                         </tbody>
                                                      </table>
                                                   </div>
                                                   <!--[if mso | IE]>
                                                </td>
                                             </tr>
                                          </table>
                                       </td>
                                    </tr>
                                 </table>
                                 <![endif]-->
                              </td>
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <!--[if mso | IE]>
               </td>
            </tr>
         </table>
         <![endif]-->
      </div>
   </body>
</html>

<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<title>Axis Expert</title>
		<!--[if !mso]><!-- -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!--<![endif]-->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body style="background-color:#eeeeee; padding: 20px;">
		<h2><?= $objet; ?> ( Contact )</h2>
		
		<h3>Infos Client : </h3>
		<table border="1" width="500px">
			<tr>
				<td>Je suis </td>
				<td><?= $type_person; ?></td>
			</tr>
			<tr>
				<td>Nom</td>
				<td><?= $nom; ?></td>
			</tr>
			<tr>
				<td>Prénom</td>
				<td><?= $prenom; ?></td>
			</tr>
			<tr>
				<td>Email</td>
				<td><?= $email; ?></td>
			</tr>
			<tr>
				<td>Téléphone</td>
				<td><?= $telephone; ?></td>
			</tr>
			<tr>
				<td>Pays</td>
				<td><?= $pays; ?></td>
			</tr>
			<tr>
				<td>Ville</td>
				<td><?= $ville; ?></td>
			</tr>
			<tr>
				<td>Inscription à la newsletter</td>
				<td><?php if($newsletter){ echo 'Oui';} else { echo 'Non';} ?></td>
			</tr>
			<tr>
				<td>Message</td>
				<td><?= $message; ?></td>
			</tr>
		</table>
		<p>-------------</p>
		<p>Depuis le formulaire de contact </p>
	</body>
</html>
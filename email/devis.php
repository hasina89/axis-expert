<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
	<head>
		<title>Axis Expert</title>
		<!--[if !mso]><!-- -->
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<!--<![endif]-->
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body style="background-color:#eeeeee; padding: 20px;">

		<h2>Demande de devis</h2>
		
		<h3>Infos Client : </h3>
		<table border="1" width="500px">
			<tr>
				<td>Nom</td>
				<td><?= $nom; ?></td>
			</tr>
            <?php /*
			<tr>
				<td>Prénom</td>
				<td><?= $prenom; ?></td>
			</tr>*/ ?>
			<tr>
				<td>Email</td>
				<td><?= $email; ?></td>
			</tr>
            <?php /*
			<tr>
				<td>Téléphone</td>
				<td><?= $telephone; ?></td>
			</tr> */ ?>
			<tr>
				<td>Adresse</td>
				<td><?= $adresse; ?></td>
			</tr>
			<tr>
				<td>Commentaire</td>
				<td><?= $comment; ?></td>
			</tr>
		</table>
		
		<h3>Infos Devis :</h3>
		<table border="1" width="500px">
			<tr>
				<td>Type de mission</td>
				<td><?= get_post_info($type_mission); ?></td>
			</tr>
			<tr>
				<td>Type immeuble</td>
				<td><?= get_post_info($type_immeuble); ?></td>
			</tr>
			<tr>
				<td>Chambre</td>
				<td><?= $nb_chambre; ?></td>
			</tr>	
			<tr>
				<td>Salle de bain</td>
				<td><?= $nb_salle_bain; ?></td>
			</tr>
			<tr>
				<td>Surface</td>
				<td><?= $surface; ?> m²</td>
			</tr>

			<?php if($avec_options == 'oui'): ?>
			<tr>
				<td>Autres options</td>
				<td>
					<?php 
						foreach ($all_options as $all_option) {
							echo $all_option['name']. ' ('. $all_option['prix'] .' €), <br>';
						} 
					?>
				</td>
			</tr>
			<?php endif; ?>

		</table>

		<h3>Prix (en Euro TVAC)</h3>
		<table border="1" width="500px">
			<tr>
				<td>Prix du bien</td>
				<td>Prix Options</td>
				<td>Prix Total</td>
			</tr>
			<tr>
				<td><?= $prix_bien; ?> €</td>
				<td><?= $total_prix_options; ?> €</td>
				<td><strong><?= $grand_total; ?> €</strong></td>
			</tr>
		</table>

		<p>-------------</p>
		<p>Axis Expert</p>

	</body>
</html>
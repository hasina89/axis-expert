<?php 
/**
 * Template Name: Page a propos
 */
 ?>

<?php get_header(); ?>
	<main class="apropos">
		<?php while ( have_posts() ) : the_post();?>
	    <section class="blcCareer blc_page">
	        <div class="container">
	            <div class="introText">
	                <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms"><?php the_field('introduction'); ?></h2>
	                <div class="wow fadeInUp" data-wow-delay="800ms">
	                    <p><?php the_field('text'); ?></p>
	                </div>
	            </div>
	        </div>         
	    </section>
	    <?php if( have_rows('listes_etapes') ): ?>
	    <section class="blcStep">
	        <div class="container">
	            <div class="listStep">
					<?php while ( have_rows('listes_etapes') ) : the_row(); ?>
	                <div class="item clr">
	                    <div class="content <?php the_sub_field('emplacement'); ?>">
	                        <div class="titleStep wow fadeIn" data-wow-delay="<?php the_sub_field('dure_animation'); ?>ms" ><?php the_sub_field('titre_etapes'); ?></div>
	                        <div class="year" ><div class="wow fadeIn" data-wow-delay="<?php the_sub_field('dure_animation'); ?>ms"><?php the_sub_field('annees'); ?></div></div>
	                        <div class="circle"><span></span></div>
	                        <div class="icone"><img src="<?php the_sub_field('icone'); ?>"></div>
	                    </div>         
	                </div>
					<?php endwhile; ?>
	            </div>
	        </div>
	    </section>
	    <?php endif; ?>
	    <section class="proposition">
	        <div class="container text">
	        	<?php $proposition = get_field('proposition'); ?>
	            <h2 class="s-titre"><?php echo $proposition['titre']; ?> :</h2>
	            <?php echo $proposition['text']; ?>            
	        </div>
	    </section>
	    <section class="slideApropos">
	        <div class="blcLeft">
	            <h2 class="titre"><?php the_field('titre_s'); ?></h2>
	            <div class="arrowApropos"></div>
	        </div>
	        <?php if( have_rows('listes_slides') ): ?>
	        <div class="listServcPage clr" id="slideApropos">
	        	<?php while ( have_rows('listes_slides') ) : the_row(); ?>
	            <div class="item" style="background-image: url(<?php the_sub_field('image'); ?>);">
	                <div class="contentSlide"><?php the_sub_field('contenu'); ?></div>
	            </div>
	        	<?php endwhile; ?>
	        </div>
	        <?php endif; ?>
	    </section>
	    <?php endwhile; ?>
	</main>
<?php get_footer(); ?>
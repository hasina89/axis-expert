var jQuery = jQuery.noConflict();
jQuery(document).ready(function () {
	new WOW().init();
	// MENU MOBILE //
	jQuery(".wrapMenuMobile").click(function () {
		jQuery(this).toggleClass('active');
		jQuery(".menuMobile").toggleClass('active');
		jQuery(".menu>ul").fadeToggle();
	});
	
	jQuery('.blcFormulaire .item').click(function(){
	  	jQuery('.blcFormulaire .item').find('input[type=radio]').attr("checked",false);
	  	jQuery(this).find('input[type=radio]').attr("checked",true);
	  	var ancestor = jQuery(this).find('input[type=radio]').attr("checked",true);
	  	var length = jQuery('input[type=radio]').length;
	  	if (length>0){
	  		jQuery('.blcFormulaire .item').removeClass('active');
	  		ancestor.parent('.item').addClass('active');
	  	}
	})
  	jQuery('.choixMeuble').click(function(){
  		jQuery('.choixMeuble').find('input[type=radio]').attr("checked",false);
  		jQuery(this).find('input[type=radio]').attr("checked",true);
  		var vis = jQuery(this).find('input[type=radio]');
  		var cl = vis.attr("id")
  		if (cl == "non_meuble") {
	  		jQuery('.blcImmeuble').removeClass('meuble');
	  		jQuery('.blcImmeuble').addClass(cl)
  		}else if (cl == "meuble"){
  			jQuery('.blcImmeuble').removeClass('non_meuble');
	  		jQuery('.blcImmeuble').addClass(cl)
  		}	  	
  	});
	jQuery('.btnObjet').click(function(){
		jQuery('.step1').addClass('show');
	});
  	jQuery('.blcFormulaire .step1 .item').click(function(){
		jQuery('.step2').addClass('show');
   	});
	jQuery('.blcFormulaire .step2 .item').click(function(){
		jQuery('.step3').addClass('show');
	});
	jQuery('.blcFormulaire .step3').click(function(){
		jQuery('.step4').addClass('show');
	});
	jQuery('.blcFormulaire .step4').click(function(){
		jQuery('.step5').addClass('show');
	});
	jQuery("body").click(function (e) {
		var target = jQuery(e.target);
		if (target.is('.menu-item-has-children a')) {
			jQuery('.header').toggleClass('active');
		}
		else {
			jQuery('.header').removeClass('active')
		}
	})
	function menuMobile() {
		jQuery(".menu li").click(function () {
			w = jQuery(window).width()
			if (w < 1200) {
				jQuery(this).find('.sub').slideToggle();
				jQuery(this).toggleClass('active')
			}
		});
	}
	menuMobile();
	// MENU FIXED //
	jQuery(window).scroll(function () {
		var posScroll = jQuery(document).scrollTop();
		if (posScroll > 50) {
			jQuery('.blcTop').addClass('sticky')
		} else {
			jQuery('.blcTop').removeClass('sticky')
		}
		var h_banner = jQuery(".header").height();
		if (jQuery(window).scrollTop() >= h_banner) {
			jQuery(".blcButton").addClass("fixed");
		} else {
			jQuery(".blcButton").removeClass("fixed");
		}
	});
	// MENU STICKY //
	var position = jQuery(window).scrollTop(); 
	// should start at 0
	jQuery(window).scroll(function() {
	    var scroll = jQuery(window).scrollTop();
	    if(scroll > position) {
	        jQuery('.blcAvis').addClass('off');
	    }
	    position = scroll;
	});
	jQuery(".blcAvis .off").click(function () {	
		jQuery('.blcAvis').removeClass('off');
	})
	// SCROLL //
	jQuery(".scroll").click(function () {
		var c = jQuery(this).attr("href");
		jQuery('html, body').animate({
			scrollTop: jQuery("#" + c).offset().top
		}, 1000, "linear");
		return false;
	});
});
function slideService() {
	w = jQuery(window).width()
	if (w > 1025) {
		if (jQuery('#slideService').hasClass('slick-initialized')) {
			jQuery('#slideService').slick("unslick");
		}
	} else {
		var initPart = jQuery('#slideService').hasClass('slick-initialized');
		if (!initPart) {
			jQuery('#slideService').slick({
				dots: false,
				infinite: true,
				autoplaySpeed: 4000,
				speed: 1000,
				slidesToShow: 1,
				arrows: true,
				autoplay: true,
				pauseOnHover: false,
				cssEase: 'linear',
				fade: true
			});
		}
		jQuery('#slideService').slick('slickRemove', 3)
	};
}
function slideCoord() {
	w = jQuery(window).width()
	if (w > 1025) {
		if (jQuery('#slideCoord').hasClass('slick-initialized')) {
			jQuery('#slideCoord').slick("unslick");
		}
	} else {
		var initPart = jQuery('#slideCoord').hasClass('slick-initialized');
		if (!initPart) {
			jQuery('#slideCoord').slick({
				dots: true,
				infinite: true,
				autoplaySpeed: 4000,
				speed: 1000,
				slidesToShow: 3,
				slidesToScroll: 1,
				arrows: false,
				autoplay: true,
				pauseOnHover: false,
				responsive: [{
						breakpoint: 769,
						settings: {
							slidesToShow: 2,
							dots: true,
						}
					},
					{
						breakpoint: 601,
						settings: {
							slidesToShow: 1,
							fade: true,
							dots: true,
						}
					},
				]
			});
		}
	};
}
jQuery(window).resize(function () {
	slideService();
	slideCoord()
});
jQuery(document).ready(function ($) {
	slideService();
	slideCoord()
	jQuery('.videoPoster').click(function () {
		jQuery(this).addClass('hide')
	// 	var vid = jQuery('#video');
	// 	vid[0].paused ? vid[0].play() : vid[0].pause();
	});
	
	jQuery('#slidePartenaire').slick({
		dots: false,
		infinite: true,
		autoplaySpeed: 4000,
		speed: 1000,
		slidesToShow: 5,
		slidesToScroll: 1,
		arrows: true,
		autoplay: true,
		pauseOnHover: false,
		responsive: [{
				breakpoint: 1025,
				settings: {
					slidesToShow: 4,
				}
			},
			{
				breakpoint: 980,
				settings: {
					slidesToShow: 3,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 481,
				settings: {
					slidesToShow: 1,
					fade:true
				}
			},
		]
	});
	jQuery('#slideApropos').slick({
		dots: false,
		infinite: true,
		autoplaySpeed: 2000,
		speed: 1000,
		slidesToShow: 3,
		slidesToScroll: 1,
		arrows: true,
		autoplay: true,
		pauseOnHover: false,
		appendArrows: jQuery('.arrowApropos'),
		responsive: [{
				breakpoint: 1025,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 768,
				settings: {
					slidesToShow: 1,
				}
			},
			{
				breakpoint: 481,
				settings: {
					slidesToShow: 1,
					fade:true
				}
			},
		]
	});
	jQuery('#slideTestimonial').slick({
		dots: false,
		infinite: true,
		autoplaySpeed: 4000,
		speed: 1000,
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: true,
		autoplay: true,
		pauseOnHover: false,
		fade:true,
	});

	jQuery('#autreActus').slick({
		dots: false,
		infinite: true,
		autoplaySpeed: 2000,
		speed: 1000,
		slidesToShow: 2,
		slidesToScroll: 1,
		arrows: true,
		autoplay: true,
		pauseOnHover: false,
		responsive: [{
			breakpoint:681,
			settings: {
				slidesToShow: 1,
				fade:true
			}
		},
		]
	});


	/* DEVIS */
	jQuery('.next1').click(function(){
		jQuery('.etape1').next(".etape2").addClass('show');
	})
	jQuery('.next2').click(function(){
		jQuery('.etape2').next(".etape3").addClass('show');
	})
	jQuery('.next3').click(function(){
		jQuery('.etape3').next(".etape4").addClass('show');
	})
	jQuery('.next4').click(function(){
	   jQuery('.etape4').next(".etape5").addClass('show');
	})
	jQuery('.next5').click(function(){
	   jQuery('.etape5').next(".etape6").addClass('show');
	});
	jQuery('.next6').click(function(){
	   jQuery('.etape6').next(".etape7").addClass('show');
	});

	//**//
	/* implémentation des fonctionnalités selon: https://www.dropbox.com/t/G6vcy53VLInfOSJ7 */
	//**//
	/* 
	Type de mission
		Etat des lieux d'entrée: 			 id 399
		Etat des lieux de sortie:   	 id 401
		Estimation d'immeuble: 	  		 id 402
		Réception provisoire: 	 			 id 403
		Etats des lieux avant travaux: id 404
		Recolement après travaux:      id 405

	Type d'immeuble 
		Appartement Studio 						id 406
		Maison 												id 418
		Bureau 												id 437
		Industriel Batim Public 			id 440
		Kot 													id 443
		Entrepot 											id 446
		Com 													id 449
		Voirie 												id 679
		Façade 												id 680
		Façades et pièces...					id 681
		Autres...		

	Autres options 
		Nbr de chambres 			#o_nbr_chambre
		Nbr salles dbn 				#o_nbr_sdb
		Surface 							#o_surface, #o_info
		Jardin								#o_0
		Garage								#o_1
		Cave 									#o_2
		Meublé								#o_3
		Dépendance gardien		#o_4
		Piscine								#o_5	


	*/
	var type_mission, type_immeuble;
	$('.formulaireDevis .mission li [name=type_mission]').on('click', function(){
		type_mission = $(this).val();
		detect_mission_immeuble();
		detect_immeuble();
	});
	$('.formulaireDevis .immeuble li [name=type_immeuble]').on('click', function(){
		type_immeuble = $(this).val();
		detect_mission_immeuble();
		detect_immeuble();
	});

	// Mission
	jQuery('.formulaireDevis .mission li').click(function(){
		var vis = jQuery(this);	
		var vislabel = vis.find('label');	
		jQuery(".mission li").removeClass('open');
		vislabel.parent().addClass('open');	
		jQuery('.autre-option').hide();
		var radio = vis.find('input');
		if (radio.hasClass('without_option')) {
			if(jQuery(radio).is(':checked')){
				jQuery('.autre-option').hide();
			}else {
				jQuery('.autre-option').show();
				detect_mission_immeuble();
				detect_immeuble();
			}
		}
		else {
			jQuery('.autre-option').show();
			detect_mission_immeuble();
			detect_immeuble();
		}
		jQuery(".etape3").addClass('show');
		jQuery('html, body').animate({ scrollTop: jQuery("#etape3").offset().top - 100 }, 1000, "linear");
	});

	$('.etapeRDV .mission li [name=type_mission]').on('click', function(){
		type_mission = $(this).val();
		detect_mission_immeuble();
		detect_immeuble();
	});
	$('.etapeRDV .liste-option li [name=type_immeuble]').on('click', function(){
		type_immeuble = $(this).val();
		detect_mission_immeuble();
		detect_immeuble();
	});

	jQuery('.etapeRDV .mission li').click(function(){
		var vis = jQuery(this);	
		var vislabel = vis.find('label');	
		jQuery(".mission li").removeClass('open');
		vislabel.parent().addClass('open');	
		jQuery('.autre-option').hide();
		var radio = vis.find('input');
		if (radio.hasClass('without_option')) {
			if(jQuery(radio).is(':checked')){
				jQuery('.autre-option').hide();
			}else {
				jQuery('.autre-option').show();
				detect_mission_immeuble();
				detect_immeuble();
			}
		}
		else {
			jQuery('.autre-option').show();
			detect_mission_immeuble();
			detect_immeuble();
		}
		jQuery(".etape3").addClass('show');
		jQuery('html, body').animate({ scrollTop: jQuery("#etape3").offset().top - 100 }, 1000, "linear");
	});

		function detect_mission_immeuble(){ console.log(type_mission + ' & '+ type_immeuble);
		$('#voirie, #facade, #les-facades-et-pieces-interieures-cote-chantier').hide();
		$('#kot').show();
		if ( type_mission == '399' || type_mission == '401' ){
			$('#kot').show();
			if ( type_immeuble == '437' || type_immeuble == '440' || type_immeuble == '446' || type_immeuble == '449' ){
				$('#o_surface, #o_info').show();
				$('#o_nbr_chambre, #o_nbr_sdb, #o_0, #o_1, #o_2, #o_3, #o_4, #o_5').hide();
			}else if( type_immeuble == '443' ){
				$('#o_3').show();
				$('#o_nbr_chambre, #o_nbr_sdb, #o_0, #o_1, #o_2, #o_surface, #o_info, #o_4, #o_5').hide();
			}else{
				$('#o_nbr_chambre, #o_nbr_sdb, #o_0, #o_1, #o_2, #o_3, #o_surface, #o_info, #o_4, #o_5').show();
			}
		}else if( type_mission == '402' ){
			$('#kot').hide();
		}else if ( type_mission == '404' || type_mission == '405' ){
			$('#kot').show();
			$('#voirie, #facade, #les-facades-et-pieces-interieures-cote-chantier').show();
		}else if ( type_mission == '403' ){
			$('#kot').show();
		}
	}

	function detect_immeuble(){
		$('#numeroAppart').parent('.search').hide();
		if ( type_immeuble == '406' ){
			$('#numeroAppart').parent('.search').show();
		}else{
			$('#numeroAppart').parent('.search').hide();
		}
	}

	// Type de status
	jQuery('.status li').click(function(){
		jQuery(".etape2").addClass('show');
		jQuery('html, body').animate({ scrollTop: jQuery("#etape2").offset().top - 100 }, 1000, "linear");
	});
	// Scroll To
	jQuery(".scroll1").click(function() {
		c = jQuery(this).attr("href");
		jQuery('html, body').animate({ scrollTop: jQuery("#" + c).offset().top - 100 }, 1000, "linear");
		return false;
	})
	/*jQuery(".immeuble li a").click(function() {
		var vis = jQuery(this);	
		if(vis.parent().hasClass("open")){
			vis.parent().removeClass("open");
		}else{
			jQuery(".immeuble li a").parent().removeClass('open');
			vis.parent().addClass('open');	
		}
	});*/
	jQuery.datetimepicker.setLocale('fr');

	var heure_semaine = ['07:30', '08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00'];
	var heure_samedi = ['08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00'];


	// jQuery('.datetimepicker').datetimepicker({
	// 	format:	'd/m/y H:i',
	// 	minDate: 0,
	// 	yearStart: 2020,
	// 	disabledWeekDays: [0,6],
	// 	dayOfWeekStart: 1,
	// 	allowTimes: schedule_used,
	// 	onSelectDate: function(selectedDate){
	// 		var selected = selectedDate;
	// 		console.log(selected);
	// 	}
	// });
	var prev_dayNum;
var schedule_used = heure_semaine;  // Use the week schedule by default.

// Function to initialise the time picker input.
function initTime(){
  $('#time').datetimepicker({
    datepicker:false,
    format:'H:i',
    step:15,
    allowTimes: schedule_used
  });
}
// On load time initialisation.
initTime();

// Initialise the date input.
$('#date').datetimepicker({
  timepicker:false,
  format:'d/m/Y',
	minDate: 0,
	yearStart: 2020,
	disabledWeekDays: [0],
	dayOfWeekStart: 1,

  // On change callback
  onChangeDateTime:function(dp,$input){

    var dateVal = $input.val();
    var timeVal = $('#time').val();
    //console.log(dateVal +" - "+ (timeVal||"No Time"));

    // Because of the d/m/Y format, have to process the date a bit to get the day number.
    val = dateVal.split("/");
    var dayNum = new Date(val[2]+"/"+val[1]+"/"+val[0]).getDay();
    //console.log("dayNum: "+dayNum);

    // if dayNum is zero (sunday), use sunday schedule... Else use the week schedule.
    schedule_used = (dayNum == 6 ) ? heure_samedi : heure_semaine;

    // If the dayNum changed.
    if( prev_dayNum != dayNum  ){
      console.log("Changed day!");
      // Re-initialise datetimepicker
      $('#time').datetimepicker("destroy");
      initTime();

      // If the actual time value is not in schedule.
      if($.inArray(timeVal,schedule_used) == -1){
        console.log("Wrong time!");
        // Clear the time value.
        $('#time').val("");
        // Focus the time input so it's obvious the user has to re-select a time.
        $('#time').focus();
      }
    }
    // Keep this dayNum in memory for the next time.
    prev_dayNum = dayNum;
  }
});

	// Quantity
	jQuery(".numbers-row .button").on("click",function(){
		var jQuerybutton=jQuery(this);
		var oldValue=jQuerybutton.parent().find("input").val();
		var newVal;
		if(jQuerybutton.text()=="+"){

			if(oldValue < 5){
				newVal=parseFloat(oldValue) + 1;
			}else{
				newVal=oldValue;
			}
		}else{
			if(oldValue>1){
				newVal=parseFloat(oldValue)-1;
			}else{
				newVal=0; 
			}
		}
		jQuerybutton.parent().find("input").val(newVal);
		return false;
	})
	// COUNTER
	 jQuery('.counter').counterUp({
	    delay: 50,
	    time: 3000
	});

	/**
	 * Bouton Espace client : menu
	 */
	$("li.btn.hvr-btn > a").attr('title','Page indisponible').css("cursor","not-allowed");

	$('#more_files').on('click', function(){
		// fileInit();
		$('#spn_inputs').append('<div class="cont-file"><span>Aucun fichier sélectionné</span><input type="file" name="file[]" class="input-file"/><i> Parcourir</i><i class="reset" style="display: none">Supprimer</i></div>');
		fileInitOter();
		reset_file_upload();
	});
	// fileInit();
	fileInitOter();
	reset_file_upload();

	function fileInit(){
		$('.input-file').change(function() {
			iz=$(this)
			val=iz.val()
			par=iz.parent(".cont-file")
			txtExt=par.find("span")
			txtBroswer=par.find("i")
			txtExt.text(val) 
			txtBroswer.text("changer")
		});
	}
	function fileInitOter(){
		$('.input-file').each(function(){
			$(this).change(function() {
			$(this).parents(".cont-file").addClass('uploaded');	
			iz=$(this)
			resetBtn=iz.siblings('.reset');
			val=iz.val()
			if(val!=""){
				par=iz.parent(".cont-file")
				txtExt=par.find("span")
				txtBroswer=par.find("i")

				txtExt.text(val) 
				txtBroswer.text("changer")
				resetBtn.show()
				resetBtn.text('Suppr')
			}
		})
		})
	}

	function reset_file_upload(){
		$('.reset').each(function(){
			$('.reset').on('click', function(){
				reset = $(this);

				btnChanger = $(this).siblings('i');
				inputFile  = $(this).siblings('.input-file');
				fakeText   = $(this).siblings('span');
				btnChanger.text("Parcourir");
				inputFile.val('');
				fakeText.text("Aucun fichier sélectionné");

				if ( reset.parents('#spn_inputs').length == 1 ){
					reset.parent('.cont-file').remove();
				}else{
					reset.hide();
				}
				return false;
			});
		});
	}

	$('.reset').click(function () {
		$('.cont-file').removeClass('uploaded');
	})
	
	// Fancybox
	$('[data-fancybox="groupe1"]').fancybox();

	// Scrollbar
	jQuery('.scrollbar-inner').scrollbar();

	if( $('#video').length ){
		var iframe = $('#video')[0],
	        player = $f(iframe);

	    jQuery('.videoPoster').bind('click', function() {
	        player.api(jQuery(this).text().toLowerCase());
		});
	}
});

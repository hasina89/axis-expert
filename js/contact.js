jQuery(function($){
    // Submit form
    $('#contact_form').submit(function (e) {
        var vis = $('#send_cform');
        var forme = $('#contact_form');  
        var txtButton = vis.text();
        var container = $('#pp-confirmation .content');
        $.ajax(ajaxurl, {     
            data: { 
                'action' : 'contact', 
                'data' : forme.serialize()             
            },            
            type:"POST",
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
                container.empty();
            },
            success:function(data){                
                // console.log(data);
                vis.text(txtButton);
                var stringData;
                stringData = $.parseJSON(data);
                container.empty();
                forme.find('.reset').trigger('click');
                grecaptcha.reset();
                container.html(stringData.msg); 
                $("a#topopup").trigger("click");
            }
        });
        return false;
    });
});
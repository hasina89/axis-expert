

jQuery(function($){



    $('.open-popup-link').magnificPopup({

        type:'inline',

        midClick: true 

    });



    // Submit form

	$('#confirm_devis').click(function (e) {

        var vis = $(this);

        var txtButton = vis.text();

        var container = $('#pp-confirmation .content');

        var forme = $('#form_devis');   

        $.ajax(ajaxurl, {     

            data: { 

                'action' : 'devis', 

                'data' : forme.serialize()             

            },            

            type:"POST",

            beforeSend:function(){

                vis.text('Veuillez patienter ...');

                container.empty();

            },
            complete:function(){
                vis.text(txtButton);
            },
            success:function(data){                

                // console.log(data);

            	var stringData;

            	stringData = $.parseJSON(data);

                container.empty();

                container.html(stringData.msg); 

                $("a#topopup").trigger("click");

            }

        });

        return false;

    });

    // finaliser devis 
    $('#finaliser_devis').click(function(){
        var vis = $(this);
        var txtButton = vis.text();
        var container = $('#pp-confirmation .content');

        $('[name=name]').val( $('#text_nom').text() );
        $('[name=type_mission]').val( $('#text_mission').text() );
        $('[name=type_immeuble]').val( $('#text_immeuble').text() );
        $('[name=nb_chambre]').val( $('#text_nb_chambre').text() );
        $('[name=nb_salle_bain]').val( $('#text_sdb').text() );
        $('[name=surface]').val( $('#text_surface').text() );
        $('[name=comment]').val( $('#text_com').text() );
        $('[name=adresse]').val( $('#text_adresse').text() );
        $('[name=email]').val( $('#text_email').text() );
        $('[name=honoraire]').val( $('#text_honoraire').text() );
        $('[name=options]').val( $('#text_options').text() );
        $('[name=remise]').val( $('#text_remise').text() );

        var form_data = new FormData();

        var files_data = jQuery('.input-file');
        var files = [];
        var total_upload = $('input[type=file]').length;

        for( var i = 0; i < total_upload; i++){
            if ( jQuery('.input-file')[i].value != "" ){
                form_data.append('files['+i+']', jQuery('.input-file')[i].files[0] );
            }
        }

        var status          = $('[name=status]:checked').val();
        var type_mission    = $('[name=type_mission]').val();
        var type_immeuble   = $('[name=type_immeuble]').val();
        var nb_chambre      = $('[name=nb_chambre]').val();
        var nb_salle_bain   = $('[name=nb_salle_bain]').val();
        var surface         = $('[name=surface]').val();
        var comment         = $('[name=comment]').val();
        var adresse         = $('[name=adresse]').val();
        var heure_devis      = $('[name=heure_devis]').val();
        var date_devis      = $('[name=date_devis]').val();
        var name            = $('[name=name]').val();
        var email           = $('[name=email]').val();
        var honoraire        = $('[name=honoraire]').val();
        var options        = $('[name=options]').val();
        var reduction        = $('[name=remise]').val();

        form_data.append('action', 'finaliser_devis');
        form_data.append('status', status);
        form_data.append('type_mission', type_mission);
        form_data.append('type_immeuble', type_immeuble);
        form_data.append('nb_chambre', nb_chambre);
        form_data.append('nb_salle_bain', nb_salle_bain);
        form_data.append('surface', surface);
        form_data.append('options', options);
        form_data.append('comment', comment);
        form_data.append('adresse', adresse);
        form_data.append('heure_devis', heure_devis);
        form_data.append('date_devis', date_devis);
        form_data.append('name', name);
        form_data.append('email', email);
        form_data.append('honoraire', honoraire);
        form_data.append('reduction', reduction);

        $.ajax(ajaxurl, {
           data: form_data,           
             contentType: false,
            processData: false,
            type:"POST",
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
                container.empty();
            },
            complete:function(){
                vis.text(txtButton);
            },
            success:function(data){                

                // console.log(data);

                var stringData;

                stringData = $.parseJSON(data);

                container.empty();

                container.html(stringData.msg); 

                $("a#topopup").trigger("click");

            }
        });

        return false;
    });



});
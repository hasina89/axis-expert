
jQuery(function($){

    $('.open-popup-link').magnificPopup({
        type:'inline',
        midClick: true 
    });

    // $('#more_files').on('click', function(){
    //     $('#spn_inputs').append('<div class="cont-file"><span>Aucun fichier sélectionné</span><input type="file" name="file[]" class="input-file"/><i class="btn hvr-btn "> Parcourir</i></div>');

    //     return false;
    // });

    // Submit form
	$('#confirm_rdv').click(function (e) {
        var vis = $(this);
        var txtButton = vis.text();
        var container = $('#pp-confirmation .content');
        var forme = $('#form_devis');   

        var form_data = new FormData();

        var querytype = jQuery('.support-query').val();
        var files_data = jQuery('.input-file');

        var files = [];
        var options = [];
        var total_upload = $('input[type=file]').length;
        
        $("input:checkbox[name='options[]']:checked").each(function(){
            options.push($(this).val());
        });

        for( var i = 0; i < total_upload; i++){
            if ( jQuery('.input-file')[i].value != "" ){
                form_data.append('files['+i+']', jQuery('.input-file')[i].files[0] );
            }
        }

        var status          = $('[name=status]').val();
        var type_mission    = $('[name=type_mission]:checked').val();
        var type_immeuble   = $('[name=type_immeuble]:checked').val();
        var nb_chambre      = $('[name=nb_chambre]').val();
        var nb_salle_bain   = $('[name=nb_salle_bain]').val();
        var surface         = $('[name=surface]').val();
        var comment         = $('[name=comment]').val();
        var adresse         = $('[name=adresse]').val();
        var numeroAppart    = $('#numeroAppart').val();
        var myfile          = $('[name=myfile]').val();
        var date_rdv        = $('[name=date_rdv]').val();
        var heure_rdv       = $('[name=heure_rdv]').val();
        var name            = $('[name=name]').val();
        var email           = $('[name=email]').val();
        var form_type       = $('[name=form_type]').val();       
        var numero          = $('#numero').val();       

        // form_data.append('files', fls);
        form_data.append('action', 'rdv');
        form_data.append('status', status);
        form_data.append('type_mission', type_mission);
        form_data.append('type_immeuble', type_immeuble);
        form_data.append('nb_chambre', nb_chambre);
        form_data.append('nb_salle_bain', nb_salle_bain);
        form_data.append('surface', surface);
        form_data.append('options', options);
        form_data.append('comment', comment);
        form_data.append('adresse', adresse);        
        form_data.append('numeroAppart', numeroAppart);        
        form_data.append('date_rdv', date_rdv);
        form_data.append('heure_rdv', heure_rdv);
        form_data.append('name', name);
        form_data.append('email', email);
        form_data.append('form_type', form_type);
        form_data.append('numero', numero);

        $.ajax(ajaxurl, {     
            data: form_data,           
             contentType: false,
            processData: false,
            type:"POST",
            beforeSend:function(){
                vis.text('Veuillez patienter ...');
                container.empty();
            },
            complete:function(){
                vis.text(txtButton);
            },
            success:function(data){                
                // console.log(data);            	
            	var stringData;
            	stringData = $.parseJSON(data);
                container.empty();
                container.html(stringData.msg); 
                $("a#topopup").trigger("click");
            }
        });
        return false;
    });

});

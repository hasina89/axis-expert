<?php 
/**
 * Template Name: Page actualite détails
 */
 ?>

<?php get_header(); ?>
	<main class="actualite">
		<section class="blcActus details">
			<div class="container">
				<div class="detail-actus ">
					<div class="content">
						<div class="blc-txt clr wow fadeInLeft" data-wow-delay="800ms">
							<h2>Lorem ipsum dolor sit amet consetetur adispiscing elit sed do</h2>
							<span class="date">12 Décembre 2020</span>
							<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud <strong>exercitation </strong>ullamco laboris nisi ut aliquip ex ea commodo
							consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
							cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
							proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>

							<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
							tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
							quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
							consequat. </p>
							</div>
							<div class="blc-img wow fadeInRight" data-wow-delay="800ms">
								<div class="img">
									<img src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/img-details.jpg" alt="Actualités">
								</div>
							</div>
					</div>
					<div class="clr"></div>
					<div class="cont-txt clr wow fadeInUp" data-wow-delay="800ms">
						<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud <strong>exercitation</strong> ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

						<p> Lorem ipsum dolor sit amet, <strong>consectetur adipisicing</strong> elit, sed do eiusmod
						tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
						quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
						consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
						cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
						proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
					</div>			
				</div>
				<div class="AutreActus">
					<h2>Autres actualités</h2>
					<div class="lst-Autre-actus" id="autreActus">
						<div class="item">
							<div class="content">
								<div class="img">
									<img src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/img-actu5.jpg" alt="Actualités">
								</div>
								<div class="titre">
									<h2>Lorem ipsum dolor sit amet, consectur adipising elit</h2>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="img">
									<img src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/img-details.jpg" alt="Actualités">
								</div>
								<div class="titre">
									<h2>Lorem ipsum dolor sit amet, consectur adipising elit</h2>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="content">
								<div class="img">
									<img src="http://axis-expert.maki-group.mg/wp-content/themes/axis/images/img-actu5.jpg" alt="Actualités">
								</div>
								<div class="titre">
									<h2>Lorem ipsum dolor sit amet, consectur adipising elit</h2>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</section>
	</main>
<?php get_footer(); ?>
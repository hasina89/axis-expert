<?php 
/**
 * Template Name: Page Tarif
 */
 ?>

<?php get_header(); ?>
<main>
    <?php while ( have_posts() ) : the_post();?>
        <section class="blcCareer blc_page blcTarif">
            <div class="container">
                <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms">Nos tarifs</h2>
                <?php 
                    $missions = array('399','401','403','402','404','405');
                    $j = 0;
                    foreach ( $missions as $id_mission ){
                        $j++;
                ?>
                        <div class="entrer wow fadeInUp" data-wow-delay="800ms">
                            <span>
                                <?php
                                    $titre = '';
                                    if( $id_mission == "399" ){
                                        $titre = 'HONORAIRES ETATS DES LIEUX LOCATIFS A L’ENTREE POUR CHAQUE PARTIE';
                                    }elseif( $id_mission == "402" ){
                                        $titre = 'HONORAIRES D\'ESTIMATION D\'IMMEUBLE';
                                    }elseif( $id_mission == "403" ){
                                        $titre = 'HONORAIRES RECEPTION PROVISOIRE';
                                    }elseif( $id_mission == "404" ){
                                        $titre = 'ETATS DES LIEUX AVANT TRAVAUX';
                                    }elseif( $id_mission == "405" ){
                                        $titre = 'RECOLEMENT APRES TRAVAUX';
                                    }elseif( $id_mission == "401"){
                                        $titre = 'HONORAIRES ETATS DES LIEUX LOCATIFS A LA SORTIE POUR CHAQUE PARTIE';
                                    }

                                    echo $titre;
                                ?>
                                
                            </span>
                        </div>
                        <div class="blocTable tab<?= $j ?> scrollbar-inner">
                            <table class="table<?= $j ?>">
                                <thead>
                                    <tr>
                                        <th>Type d'immeuble</th>
                                        <th>Tarification</th>
                                        <th>Prix</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $tarifs = get_field('liste_immeuble', $id_mission);
                                        $option = get_field('avec_options',$id_mission);
                                        if ( $tarifs ):
                                            foreach ( $tarifs as $tarif ):
                                                    if ( $tarif['tarif'] == 'avec_prix' ):
                                                        $liste_chambres = $tarif['liste_chambre'];
                                                        if ( is_array( $liste_chambres )):
                                                            foreach ($liste_chambres as $ch ):
                                                                if ( $ch['prix_chambre'] != '-1' ):
                                            ?>
                                                                <tr>
                                                                    <td class="bold"><?= get_post_info( $tarif['item_immeuble'] ) .' '.$ch['nombre_chambre'] .' chambre'?><?php if( $ch['nombre_chambre'] > 1 ) echo "s" ?></td>                                        
                                                                    <td>Par chambre</td>                                                                
                                                                    <td><?= $ch['prix_chambre'] .' €' ?></td>
                                                                </tr>
                                                                <?php endif; ?>
                                            <?php 
                                                            endforeach;
                                                        endif;
                                                    elseif ( $tarif['tarif'] == "surface" ):
                                                ?>
                                                    <tr>
                                                        <td class="bold"><?= get_post_info( $tarif['item_immeuble'] ) ?></td>
                                                        <td>Selon surface</td>
                                                        <td><?= $tarif['prix_de_base'] ?> €</td>
                                                    </tr>
                                                <?php
                                                    elseif( $tarif['tarif'] == "sur_devis" ):
                                                        ?>
                                                    <tr>
                                                        <td class="bold"><?= get_post_info( $tarif['item_immeuble'] ) ?></td>
                                                        <td>Sur Devis</td>
                                                        <td>-</td>
                                                    </tr>
                                                <?php
                                                    elseif( $tarif['tarif'] == "prix_unique" ):
                                                ?>
                                                    <tr>
                                                        <td class="bold"><?= get_post_info( $tarif['item_immeuble'] ) ?></td>
                                                        <td>Prix unique</td>
                                                        <td><?= $tarif['unique_prix'] ?> €</td>
                                                    </tr>
                                                <?php
                                                    endif;
                                            endforeach;
                                        endif;
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php } ?>
                <div class="entrer wow fadeInUp" data-wow-delay="800ms">
                    <span>
                        LES OPTIONS
                    </span>
                </div>
                <div class="blocTable">
                    <table class="table<?= $j ?>">
                        <thead>
                            <tr>
                                <th>Options</th>
                                <th>Type de mission</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                            $args = array(
                                'post_type' => 'autre_option',
                            );
                            $opts = new WP_Query( $args );
                            while( $opts->have_posts() ):
                                $opts->the_post();
                                $tarif_chambres = get_field('tarif_chambre');
                        ?>
                            
                                <tr>
                                    <td class="bold"><?php the_title(); ?></td>
                                    <td>
                                        <?php foreach( $tarif_chambres as $tariff ): ?>
                                            <div style="display: flex; justify-content: center">
                                                <div><?= get_post_info($tariff['liste_mission_option']) ?> : </div>
                                                <?php if ( $tariff['prix_option'] > 0 ) : ?>
                                                    <div>&nbsp; <?= $tariff['prix_option'] ?> €</div>
                                                <?php else: ?>
                                                    <div>&nbsp; Inclus </div>
                                                <?php endif; ?>
                                            </div>
                                    <?php endforeach; ?>
                                    </td>
                                    
                                </tr>
                            <?php
                                wp_reset_postdata();
                             endwhile; ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </section>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>
<?php get_header(); ?>
    <main class="actualite">
        <section class="blcActus details">
            <div class="container">
                <?php 
                    $current_id = 0;
                    if ( have_posts() ): 
                        while ( have_posts() ):
                            $current_id = $post->ID;
                            the_post();
                            $contents = get_field('texte_content');
                     ?>
                <div class="detail-actus ">
                    <div class="content">
                            <div class="blc-txt clr wow fadeInLeft" data-wow-delay="800ms">
                                <h2><?php the_title() ?></h2>
                                <span class="date"><?php the_date() ?></span>
                                <?php the_content(); ?>
                            </div>                        
                            <div class="blc-img wow fadeInRight" data-wow-delay="800ms">
                                <div class="img">
                                <?php
                                    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
                                    if  ( ! empty( $featured_image_url ) ) {

                                        echo get_the_post_thumbnail(  $current_id, 'img-single', array('alt' => $post->ID) );

                                    }else{
                                        echo "<img src='".get_template_directory_uri() . "/images/placeholder.png' alt='' />";
                                    }
                                ?>
                                </div>
                            </div>
                        
                    </div>
                    <div class="clr"></div>
                    <?php 
                        if( is_array( $contents) ): 
                            foreach ($contents as $cont ):
                    ?>
                            <div class="cont-txt clr wow fadeInUp" data-wow-delay="800ms">
                             <?= $cont['texte_single'] ?>
                            </div>
                    <?php endforeach; endif; ?>
                </div>
                <?php endwhile; wp_reset_postdata(); endif; ?>
                <div class="AutreActus">
                    <h2>Autres actualités</h2>
                    <div class="lst-Actu clr lst-Autre-actus" id="autreActus">
                        <?php
                            $total_autres = get_field('autres_actualites', $current_id) ? get_field('autres_actualites', $current_id) : 3;
                            $args = array(
                                'post_type'      => 'post',
                                'posts_per_page' => $total_autres,
                                'post__not_in'   => array( $current_id ),
                            );
                            $q = new WP_Query($args);
                        ?>
                        <?php 
                            $i = 0;                            
                            while( $q->have_posts() ):
                                $q->the_post();
                        ?>

                                <div class="item">
                                    <div class="content">
                                        <a href="<?php the_permalink() ?>" title="">
                                            <div class="img">
                                                <?php
                                                    $featured_image_url = wp_get_attachment_url( get_post_thumbnail_id( get_the_ID() ) );
                                                    if  ( ! empty( $featured_image_url ) ) {

                                                        echo get_the_post_thumbnail( $post->ID, 'img-autres-actu', array('alt' => $post->ID) );

                                                    }else{
                                                        echo "<img src='".get_template_directory_uri() . "/images/placeholder.png' alt='' />";
                                                    }
                                                 ?>
                                                <div class="hover">
                                                    <div class="btn-h">
                                                        <a href="<?php the_permalink() ?>" title="<?php the_title() ?>" class="btn hvr-btn">En savoir plus</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="txt">
                                                <h2  class="titre"><?php the_title() ?></h2>
                                                <span class="date"><?php the_date() ?></span>
                                            </div>
                                        </a>
                                    </div>
                                </div>

                            <?php $i++; endwhile;  wp_reset_postdata(); ?>

                    </div>
                </div>
            </div>
        </section>
    </main>
<?php get_footer(); ?>
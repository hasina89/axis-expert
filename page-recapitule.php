<?php 
/**
 * Template Name: Page Recapitulatif
 */
    
   $par_partie = '';

    $url = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
    $url_components = parse_url($url);
    parse_str($url_components['query'], $params); 
    
    if ( isset($params['nom']) && !empty( $params['nom']) ){
        $nom = $params['nom'];
    }else{
        $nom = '';
    }
    if ( isset($params['email']) && !empty( $params['email']) ){
        $email = $params['email'];
    }else{
        $email = '';
    }
    if ( isset($params['adresse']) && !empty( $params['adresse']) ){
        $adresse =$params['adresse'];
    }else{
        $adresse = '';
    }
    if ( isset($params['coms']) && !empty( $params['coms']) ){
        $coms = $params['coms'];
    }else{
        $coms = '';
    }
    if ( isset($params['mission']) && !empty( $params['mission']) ){
        $mission = $params['mission'] ;
    }else{
        $mission = '';
    }
    if ( isset($params['immeuble']) && !empty( $params['immeuble']) ){
        $immeuble = $params['immeuble'];
    }else{
        $immeuble = '';
    }
    if ( isset($params['nb_chambre']) && !empty( $params['nb_chambre']) ){
        $nb_chambre =$params['nb_chambre'];
    }else{
        $nb_chambre = '';
    }
    if ( isset($params['sdb']) && !empty( $params['sdb']) ){
        $sdb = $params['sdb'];
    }else{
        $sdb = '';
    }
    if ( isset($params['surface']) && !empty( $params['surface']) ){
        $surface = $params['surface'];
    }else{
        $surface = '';
    }
    if ( isset($params['option']) && !empty( $params['option']) ){
        $options = str_replace('|', ', ', $params['option']);
    }else{
        $options = '';
    }
    if ( isset($params['honoraire']) && !empty( $params['honoraire']) ){
        $honoraire = $params['honoraire'];
    }else{
        $honoraire = '';
    }
    if ( isset($params['rem']) && !empty( $params['rem']) ){
        $reduction = (int)$params['rem'];
    }else{
        $reduction = 0;
    }

    if( $immeuble == 'Appartement / Studio' || $immeuble == 'Maison' )
      $par_partie = ' par partie';

    if ( $honoraire != '' && $reduction > 0 ){
        $honoraire_reduit = $honoraire - ($reduction * $honoraire / 100);
    }else{
        $honoraire_reduit = 0;
    }
?>

<?php get_header(); ?>
<main>
    <?php while ( have_posts() ) : the_post();?>
        <section class="blcCareer blc_page blcRecapitule">
            <div class="container">
                <div class="introText">
                    <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms">
                        <?php echo get_field('grand_titre') ?>
                        <span><?php echo get_field('sous-titre') ?></span>
                    </h2>
                    <div class="wow fadeInUp" data-wow-delay="800ms">
                        <?= get_field('contenu_texte') ?>
                    </div>
                    <div class="coordoner">
                        <div class="stitre">
                            <h3>Vos coordonnées et votre commentaire</h3>
                        </div>
                        <ul>
                            <li>
                                <span>Nom :</span>
                                <span id="text_nom"><?= $nom ?></span></span>
                            </li>
                            <li>
                                <span>E-mail :</span>
                                <span id="text_email"><?= $email ?></span>
                            </li>
                            <li>
                                <span>Adresse :</span>
                                <span id="text_adresse"><?= $adresse ?></span>
                            </li>
                            <li class="special">
                                <span>Commentaire :</span>
                                <span id="text_com"><?= $coms ?></span>
                            </li>
                        </ul>
                        <div class="stitre">
                            <h3>Type de mission et type d’immeuble</h3>
                        </div>
                        <ul>
                            <?php if ( $mission != '' ): ?>
                            <li>
                                <span>Type de mission :</span>
                                <span id="text_mission"><?= $mission ?></span>
                            </li>
                            <?php endif;
                            if ( $immeuble != '' ): ?>
                            <li>
                                <span>Type d’immeuble : </span>
                                <span id="text_immeuble"><?= $immeuble ?></span>
                            </li>
                            <?php endif; if ( $nb_chambre != '' ): ?>
                                <li>
                                    <span>Chambre : </span>
                                    <span id="text_nb_chambre"><?= $nb_chambre ?></span>
                                </li>
                            <?php endif; 
                            if ( $sdb != '' ):
                            ?>
                                <li>
                                    <span>Salle de bain :</span>
                                    <span id="text_sdb"><?= $sdb ?></span>
                                </li>
                            <?php endif; 
                            if ( $surface != '' ) :?>
                            <li>
                                <span>Surface : </span>
                                <span id="text_surface"><?= $surface ?> m²</span>
                            </li>
                            <?php endif;
                            if ( $options != '' ): ?>
                                <li>
                                    <span>Options : </span>
                                    <span id="text_options"><?= $options ?></span>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <?php if ( $honoraire != '' && $reduction == 0 ): ?>
                            <div class="stitre">
                                <h3>Honoraires</h3>
                            </div>
                            <ul>
                                <li>
                                    <span>Honoraire de la mission :</span>
                                    <span id="text_honoraire"><?= $honoraire ?> € <?= $par_partie ?></span>
                                </li>
                            </ul>
                        <?php elseif( $honoraire != '' && $reduction > 0 ): ?>
                             <div class="stitre">
                                <h3>Honoraires & Remise</h3>
                            </div>
                            <ul>
                                <li>
                                    <span>Honoraire normale de la mission :</span>
                                    <span id="text_honoraire"><?= $honoraire ?> € <?= $par_partie ?></span>
                                </li>
                                <li>
                                    <span>Votre remise :</span>
                                    <span id="text_remise"><?= $reduction ?> %</span>
                                </li>
                                <li>
                                    <span>Honoraire remisé de la mission :</span>
                                    <span id="text_reduit"><?= $honoraire_reduit ?> € <?= $par_partie ?></span>
                                </li>
                            </ul>
                            <input type="hidden" name="remise">
                        <?php endif; ?>
                    </div>
                    <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms">
                        Autres informations nécessaires
                        <span>Que vous devez obligatoirement fournir pour votre devis</span>
                    </h2>
                    <div class="information">
                        <div class="sousInfos">
                            <div class="item item1">
                                <span>Vous êtes :</span>
                                <div class="blocRadio">
                                    <ul class="blc-check">
                                        <li>
                                            <input type="radio" name="status" id="locataire" value="locataire">
                                            <label for="locataire">Locataire</label>
                                        </li>
                                        <li>
                                            <input type="radio" name="status" id="bailleur" value="bailleur">
                                            <label for="bailleur">Bailleur</label>
                                        </li>
                                        <li>
                                            <input type="radio" name="status" id="professionnel" value="professionnel">
                                            <label for="professionnel">Professionnel</label>
                                        </li>
                                        <li>
                                            <input type="radio" name="status" id="gestionnaire" value="gestionnaire">
                                            <label for="gestionnaire">Gestionnaire</label>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="item item2">
                                <span>Date et heure souhaitées :</span>
                                <div class="chp date">
                                    <input type="text" name="date_devis" id="date" placeholder="Date et heure" class="datetimepicker">
                                    <input type="text" name="heure_devis" id="time" placeholder="" class="datetimepicker">
                                </div>
                            </div>
                        </div>
                        <div class="modifChamp">
                            <div class="blc-btn">
                                <div class="multiFichier">
                                    <div class="chp">
                                        <div class="cont-file">
                                            <span>Aucun fichier sélectionné</span>
                                            <input type="file" name="file[]" class="input-file">
                                            <i class="upload"></i>
                                            <i class="reset" style="display: none"></i>
                                        </div>
                                        <span id="spn_inputs"></span>
                                    </div>
                                    <div class="info">
                                        <p><a href="javascript:void(0);" id="more_files" class="btn hvr-btn">Ajouter d'autre fichiers</a></p><a href="javascript:void(0);" id="more_files"></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="name">
                    <input type="hidden" name="email">
                    <input type="hidden" name="adresse">
                    <input type="hidden" name="comment">
                    <input type="hidden" name="type_mission">
                    <input type="hidden" name="type_immeuble">
                    <input type="hidden" name="nb_chambre">
                    <input type="hidden" name="nb_salle_bain">
                    <input type="hidden" name="surface">
                    <input type="hidden" name="honoraire">
                    <input type="hidden" name="options">
                    <div class="blc-btn">
                        <a href="#" class="btn hvr-btn" id="finaliser_devis">Finaliser le devis</a>
                    </div>
                </div>

            </div>
        </section>
        <a href="#pp-confirmation" id="topopup" class="fancybox" data-fancybox="" style="display: none;"></a>
        <!-- popup -->
        <div class="pp-confirmation" id="pp-confirmation" style="display: none;">
            <div class="content">
                <h2>Confirmation</h2>
                <p>Votre demande de rendez-vous a été prise en compte </p>
            </div>
        </div>
        <!--# popup  -->
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>
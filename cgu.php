<?php
/**
 * Template Name: Page conditions générales
 */
?>

<?php get_header(); ?>
    <main class="cgu">
        <div class="conditionsgeneral">
            <div class="container">

                <!--<h2 class="introHeading ">Conditions générales<span></span></h2>-->

                <h2 class="introHeading "><?php the_field('titre') ?><span></span></h2>

                <div class="lst-cgu wow fadeIn clr" data-wow-delay="900ms" style="visibility: visible; animation-delay: 900ms; animation-name: fadeIn;">

                    <?php the_field('contenu') ?>

                    <!--
                    <p><strong>PREAMBULE</strong></p>


                    <p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


                    <p class="has-text-align-left">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


                    <p><strong>Article 1&nbsp;: OBJET</strong></p>


                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                        tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                        quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                        consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                        cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                        proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>


                    <ul>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
                        <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
                        <li>VLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod</li>
                        <li>Menaçant une personne ou un groupe de personne ou incitant au harcèlement.</li>
                    </ul>
                    -->

                </div>
            </div>

        </div>

    </main>
<?php get_footer(); ?>
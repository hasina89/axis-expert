<?php 
/**
 * Template Name: Page contact
 */
 ?>

<?php get_header(); ?>
<main>
	<section class="contact clr">

		<form id="contact_form" method="POST">
		    <div class="left">
		        <div class="content">
		            <div class="IntroLeft">Aidez-nous à vous diriger vers la bonne personne</div>
		            <div class="introFormulaire">Je suis :</div>
		            <div class="formLeft">
		            	<div class="blcRadio">
							<ul>
								<li> 
								  <input id="locataire" checked="" name="type_person" value="Locataire" type="radio">
								  <label for="locataire">locataire</label>
								  <div class="check"></div>     
								</li> 
								<li> 
								  <input id="proprietaire" name="type_person" value="Proprietaire"  type="radio">
								  <label for="proprietaire">proprietaire</label>
								  <div class="check"></div>     
								</li>
								<li> 
								  <input id="entrepreneur" name="type_person" value="Entrepreneur"  type="radio">
								  <label for="entrepreneur">entrepreneur</label>
								  <div class="check"></div>     
								</li>
								<li> 
								  <input id="agent-immo" name="type_person" value="Agent immobilier"  type="radio">
								  <label for="agent-immo">agent immobilier </label>
								  <div class="check"></div>     
								</li>
							</ul>  
						</div>
		            	<?php //echo do_shortcode('[hf_form slug="left-contact"]'); ?>
		            	
		            </div>
		        </div>
		    </div>
		    <div class="right">
		        <div class="blc-formulaire wow fadeIn" data-wow-delay="800ms">
		            <div class="title">
		            	<h1><?php the_field('titre_contact') ?></h1>
		            	<?php the_field('titre_contact_description') ?>
		            	<!--
		                <h1>Besoin d'un expert immobilier ?</h1>
		                <p>N'hésitez pas à nous contacter pour toute demande de devis ou pour planifier un rendez-vous avec un de nos experts immobiliers agréés.</p>
		            	-->
		            </div>
		            <div class="formRight">
		            	<div class="col">
							<div class="blc-chp">
								<div class="chp">
									<input type="text" name="nom" placeholder="Votre nom *" required >
								</div>
							</div>
							<div class="blc-chp">
								<div class="chp">
									<input type="text" name="prenom" placeholder="Votre prénom*" required >
								</div>
							</div>
							<div class="blc-chp">
								<div class="chp">
									<input type="text" name="telephone" placeholder="Votre numéro de téléphone*" required >
								</div>
							</div>
							<div class="blc-chp">
								<div class="chp">
									<input type="email" name="email" placeholder="Votre adresse e-mail*" required >
								</div>
							</div>
							<div class="blc-chp">
								<div class="chp">
									<input type="text" name="pays" placeholder="Pays">
								</div>
							</div>
						</div>
						<div class="col">
							<div class="blc-chp">
								<div class="chp">
									<input type="text" name="ville" placeholder="Ville">
								</div>
							</div>
							<div class="blc-chp">
								<div class="chp">
									<input type="text" name="objet" placeholder="Objet de votre demande *" required >
								</div>
							</div>
							<div class="blc-chp">
								<div class="chp txtarea">
									<textarea name="message" id="right-contact-textarea" placeholder="Votre demande" required ></textarea>
								</div>
							</div>
							<div class="blc-chp">
								<div class="blc-check">
									 <ul>
										<li> 
										  <input id="Newsletter" name="newsletter" value="oui" type="checkbox">
										  <label for="Newsletter">Abonnez-vous à notre Newsletter</label>
										  <div class="check"></div>     
										</li> 
									  </ul>
								</div>
							</div>
						</div>
						<div class="clear"></div>
						<div class="blc-btn">
							<div class="captcha">
								<div class="g-recaptcha" data-sitekey="6LfD570UAAAAAOcE_Byao2CAflkvs6rDcr_EIgbn"></div>
								<script src="https://www.google.com/recaptcha/api.js"></script>
							</div>
							<button class="btn hvr-btn" id="send_cform">Envoyer le message</button>
							<input type="reset" class="reset" style="display: none;">
						</div>

						<!-- popup confirmation -->
                        <a href="#pp-confirmation" id="topopup" class="fancybox" data-fancybox="" style="display: none;"></a>
                        <div class="pp-confirmation" id="pp-confirmation" style="display: none;">
                            <div class="content"></div>
                        </div>
		                <?php //echo do_shortcode('[hf_form slug="right-contact"]'); ?>
		            </div>
		        </div>
		    </div>
	    </form>
	    <div class="blc_join">
	    	<div class="rejoindre">
				<div class="title">Rejoignez-nous</div>
				<p>Devenez expert immobilier ou expert géomètre</p>
				<a href="<?php the_permalink(27); ?>" class="btn">Je postule</a>
			</div>
	    </div>
	   
	</section>
</main>
<?php get_footer(); ?>
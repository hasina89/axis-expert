<?php 

add_theme_support( 'title-tag' );
add_theme_support( 'post-thumbnails' ); 

register_nav_menus( array(
  'primaire'  => __( 'Menu Primaire', 'axis' )
) );

function axis_widgets_init() {
  register_sidebar( array(
    'name'          => __( 'Footer 1', 'axis' ),
    'id'            => 'sidebar-1',
    'description'   => __( 'Sidebar footer 1.', 'axis' ),
    'before_widget' => '<section id="%1$s" class="widget %2$s">',
    'after_widget'  => '</section>',
    'before_title'  => '<h2 class="widget-title">',
    'after_title'   => '</h2>',
  ) );
  
}
add_action( 'widgets_init', 'axis_widgets_init' );

function axis_scripts() {

    wp_enqueue_style( 'animate', get_template_directory_uri() . '/css/animate.css' );
    wp_enqueue_style( 'aos', get_template_directory_uri() . '/css/aos.css' );
    wp_enqueue_style( 'datetimepicker', get_template_directory_uri() . '/css/datetimepicker.css' );
    wp_enqueue_style( 'slick', get_template_directory_uri() . '/css/slick.css' );
    wp_enqueue_style( 'fancybox-css', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.css' );
    wp_enqueue_style( 'magnific-popup', get_template_directory_uri().'/css/magnific-popup.css' );
    wp_enqueue_style( 'reset', get_template_directory_uri() . '/css/reset.css' );
  // Load our main stylesheet.
  wp_enqueue_style( 'axis-style', get_stylesheet_uri() );

  wp_enqueue_script( 'jquery-theme', get_template_directory_uri() . '/js/jquery.min.js' );
    wp_enqueue_script( 'datetimepicker', get_template_directory_uri() . '/js/datetimepicker.js' );
    wp_enqueue_script( 'slick-min', get_template_directory_uri() . '/js/slick.min.js' );
    wp_enqueue_script( 'jquery-easing', get_template_directory_uri() . '/js/jquery.easing.js' );
    wp_enqueue_script( 'wow-min', get_template_directory_uri() . '/js/wow.min.js' );
    wp_enqueue_script( 'jquery-fancybox', get_template_directory_uri() . '/js/fancybox/jquery.fancybox.js' );
    wp_enqueue_script( 'jquery-counterup', get_template_directory_uri() . '/js/jquery.counterup.min.js' );
    wp_enqueue_script( 'jquery-ui', get_template_directory_uri() . '/js/jquery-ui.min.js' );
    wp_enqueue_script( 'map', get_template_directory_uri() . '/js/map.js' );
    wp_enqueue_script( 'waypoints-min', get_template_directory_uri() . '/js/waypoints.min.js' );
    wp_enqueue_script( 'jquery.magnific-popup', get_template_directory_uri() . '/js/jquery.magnific-popup.js' );
    wp_enqueue_script( 'input_file', get_template_directory_uri() . '/js/input_file.js' );
    wp_enqueue_script( 'frogalopp', 'https://cdn.jsdelivr.net/npm/vimeo-froogaloop-server@0.1.0/froogaloop.min.js' );
    wp_enqueue_script( 'scrollbar', get_template_directory_uri() . '/js/jquery.scrollbar.js' );
    wp_enqueue_script( 'custom', get_template_directory_uri() . '/js/custom.js' );
    wp_enqueue_script( 'slick.min', get_template_directory_uri() . '/js/slick.min.js' );
    wp_enqueue_script( 'devis', get_template_directory_uri() . '/js/devis.js' );
    wp_enqueue_script( 'contact', get_template_directory_uri() . '/js/contact.js' );
    wp_enqueue_script( 'rdv', get_template_directory_uri() . '/js/rdv.js' );

    // pass Ajax Url to script.js
    wp_localize_script( 'devis', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_localize_script( 'contact', 'ajaxurl', admin_url( 'admin-ajax.php' ) );
    wp_localize_script( 'rdv', 'ajaxurl', admin_url( 'admin-ajax.php' ) );

    
}
add_action( 'wp_enqueue_scripts', 'axis_scripts' );

/* Allow SVG */
function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
}
add_filter('upload_mimes', 'cc_mime_types');

// ACF option page
if( function_exists('acf_add_options_page') ) {  
    acf_add_options_page();  
}

// Submenu class
function overrideSubmenuClasses() {
    return array('sub');
}
add_action('nav_menu_submenu_css_class', 'overrideSubmenuClasses');


add_action('wpcf7_before_send_mail', 'save_application_form');
function save_application_form($wpcf7){
  global $wpdb;
  $submission = WPCF7_Submission::get_instance();
  $nom = $_POST['nom']; 
  $votreAvis = $_POST['votre-avis'];

  $my_post = array(
    'post_title' => $nom,
    'societe' => $societe,
    'appreciation' => $appreciation,
    'post_content' => $votreAvis,
    'post_status' => 'pending',
    'post_type' => 'avis_client',
    'post_author' => 1,
  );
  $the_post_id = wp_insert_post( $my_post );
  update_post_meta( $the_post_id, 'societe', $_POST['societe']);
  update_post_meta( $the_post_id, 'appreciation', $_POST['appreciation']);
}

add_filter( 'wpcf7_support_html5_fallback', '__return_true' );

// favicon Admin
function add_favicon() {
    $favicon_url = get_field('favicon', 'option');  
  echo '<link rel="shortcut icon" href="' . $favicon_url . '" type="image/x-icon" />';
}

// login page and admin pages  
add_action('login_head', 'add_favicon');
add_action('admin_head', 'add_favicon');
add_action('wp_head', 'add_favicon');


// add_filter('allowed_http_origins', 'add_allowed_origins');
function add_allowed_origins($origins) {
    $origins[] = site_url();
    return $origins;
}


// Helper
require_once get_template_directory() . '/inc/helper.php';
// DB
require_once get_template_directory() . '/inc/db.php';

// Handler ajax Devis & RDV
require_once get_template_directory() . '/inc/devis.php';
require_once get_template_directory() . '/inc/rdv.php';

// Handler ajax Contact
require_once get_template_directory() . '/inc/contact.php';


/* BO */
function quote_list_menu() {
    add_submenu_page( 
        'edit.php?post_type=mission',
        'Demande de devis',
        'Demande de devis',
        'manage_options',
        'quote_leads',
        'quote_list_fx'
    );

    add_submenu_page( 
        '',
        'Détail Simulation',
        'Détail Simulation',
        'manage_options',
        'pool_simulation_detail',
        'pool_detail_fx'
    );
}
// add_action('admin_menu', 'quote_list_menu'); 

function quote_list_fx(){

    global $wpdb;
    $quotes = get_leads();

    if(empty($quotes)){
        $total = 0;
    }
    else {
        $total = count($quotes);
    }

    ?>
    <style>
        .liste_news .wp-list-table { width: 99% !important; }
        .liste_news .wp-list-table thead tr{ background: #eeecec !important; }
        .alternate, .striped > tbody > tr td { border-bottom: 1px solid #f1f1f1; }
        .alternate, .striped > tbody > :nth-child(2n+1), ul.striped > :nth-child(2n+1){ background-color: #f9f9f9 !important; }
        /*.alternate, .striped > tbody > tr td.unfinished { border-left: 5px solid #f00; border-bottom: 3px solid transparent; border-top: 3px solid transparent; }*/
        .alternate, .striped > tbody > tr td.finished { border-left: 5px solid #10b805; border-bottom: 3px solid transparent; border-top: 3px solid transparent; background: rgba(16, 184, 5, 0.16); }
    </style>
    <div class="wrap">
        <h1 class="wp-heading-inline">Liste de demande de devis</h1>
        <p>Il y a <b><?= $total; ?></b> demande(s) dans votre base de données</p>
        <table class="wp-list-table widefat fixed striped">
            <thead>
                <tr>
                    <th>Nom & Prénom</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                    <th>Etape</th>
                    <th>Status</th>
                    <th>Piscine pour</th>
                    <th>Rappel</th>
                    <th>Date</th>
                </tr>
            </thead>
            
            <tbody id="the-list">
                <?php 
                foreach ($quotes as $quote) : 
                    $pool_id   = $quote->simulation_id;
                    $lead_id   = $quote->customer_id;
                    $nom       = $quote->nom .' '.$quote->prenom;
                    $email     = $quote->email;
                    $step      = $quote->step;
                    $telephone = $quote->telephone;
                    $finished  = $quote->finished;
                    $pool_time = $quote->pool_time;
                    $remind    = $quote->remind;
                    $date      = $quote->last_modify;
            
                    if ($finished == 1) {
                        $txt_finished = 'Terminé';
                        $class_finished = 'finished';
                    }else {
                        $txt_finished = 'En cours';
                        $class_finished = 'unfinished';
                    }
            
                ?>            
                    <tr>
                        <td><?= $nom; ?> <br><a href="<?= link_detail( $lead_id, $pool_id ); ?>">Voir détail</a></td>
                        <td><?= $email; ?></td>
                        <td><?= $telephone; ?></td>
                        <td><?= $step; ?></td>
                        <td class="<?= $class_finished; ?>"><?= $txt_finished; ?></td>
                        <td><?= $pool_time; ?></td>
                        <td><?= $remind; ?></td>
                        <td><?= $date; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
<?php }


/**
 * Taille des images
 */
add_action( 'after_setup_theme', 'wp_image_sizes' );
function wp_image_sizes() {
    add_image_size( 'partner-thumb', 200, 80, true );
    add_image_size( 'img-single', 565, 415, true );
    add_image_size( 'img-autres-actu', 450, 330, true );
}

function check_code_promo_public( $code_promo ){
    $cp_publics = get_field('code_promo_public','option');
    $out = array();
    if ( is_array( $cp_publics ) ){
        foreach( $cp_publics as $cpp ){
            if ( in_array( $code_promo, $cpp ) && is_cp_valide( $cpp['fin_validite'] )){
                $out['reduction'] = $cpp['taux_de_reduction_%'];
                // $out['fin']       = $cpp['fin_validite'];
                break;
            }
        }
    }

    return $out;
}

function check_if_email_partenaire( $email_partenaire ){
    $cp_parts = get_field('cp_partenaire','option');
    $out = array();
    $stop = false;
    if ( is_array( $cp_parts ) ){
        foreach( $cp_parts as $cp_part ){ 
            $codes = $cp_part['les_codes'];
            if ( is_array( $codes ) && $stop === false ){
                foreach( $codes as $code ){
                    if ( in_array( $email_partenaire, $code ) ){
                        if (  is_cp_valide( $code['fin_validite'] ) ){
                            $out['reduction'] = $code['taux_de_reduction'];
                            // $out['fin']       = $code['fin_validite'];
                            $out['entreprise']= $cp_part['entreprise'];
                            $stop = true;
                            break;
                        }
                    }
                }
            }else{
                break;
            }
        }
    }
    return $out;
}

function is_cp_valide( $deadline ){
    $deadline = DateTime::createFromFormat('d/m/Y', $deadline ); 
    $today    = DateTime::createFromFormat('d/m/Y', date('d/m/Y')); 
    $diff     = $today->diff($deadline)->format("%r%a"); 

    if ( $diff >= 0 ){
        return true;
    }else{
        return false;
    }
}

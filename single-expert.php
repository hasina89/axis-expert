<?php get_header(); ?>

    <main>
        <?php while ( have_posts() ) : the_post();?>
            <div class="container detail-career text">
                <?php the_content(); ?>
            </div>
        <?php endwhile; ?>
    </main>

<?php get_footer(); ?>
<footer >
	<div class="footer">
		<div class="container">
			<div class="footer1 wow fadeInLeft" data-wow-delay="800ms">
				<ul>
					<li><a href="<?php the_permalink(109); ?>" title="Accueil">État des lieux locatifs</a></li>
					<li><a href="<?php the_permalink(107); ?>" title="À propos">Valorisation de dégâts locatifs</a></li>
					<li><a href="<?php the_permalink(111); ?>" title="Services">Estimation d’immeubles</a></li>
					<li><a href="<?php the_permalink(113); ?>" title="Offre d'emploi">États des lieux avant travaux</a></li>
					<li><a href="<?php the_permalink(115); ?>" title="Services">Recolements après travaux</a></li>
					<li><a href="<?php the_permalink(117); ?>" title="Offre d'emploi">Réception provisoire</a></li>
				</ul>

			</div>
			<div class="footer2 wow fadeIn" data-wow-delay="800ms">
				<a href="index.php" class="logo-f"><img src="<?php the_field('footer_logo', 'option'); ?>"></a>
				<span><?php the_field('localisation', 'option'); ?></span>
				<div class="adr">
					<?php the_field('adresse', 'option'); ?>
				</div>
				<div class="contact">
					<?php $tel = get_field('telephone', 'option'); if( $tel ): ?>
						<a href="tel:<?php echo $tel['lien']; ?>" title="<?php echo $tel['afficher']; ?>"><?php echo $tel['afficher']; ?> </a>
					<?php endif; ?> 
					<i>&nbsp;  // &nbsp;  </i> <a href="mailto:<?php the_field('mail', 'option'); ?>" title="<?php the_field('mail', 'option'); ?>"> <?php the_field('mail', 'option'); ?>   </a>
				</div>
			</div>
			<div class="footer3 wow fadeInRight" data-wow-delay="800ms">
				<a href="header" class="scroll scrollTop">Top</a>
				<ul>
					<li><a href="<?php the_permalink(2); ?>" title="Accueil">Accueil</a></li>
					<li><a href="<?php the_permalink(6); ?>" title="À propos"> À propos </a></li>
					<li><a href="<?php the_permalink(27); ?>" title="Offre d'emploi">Offre d'emploi</a></li>
					<li><a href="<?php the_permalink(25); ?>" title="Documents">Documents</a></li>
					<!--<li><a href="<?php the_permalink(22); ?>" title="Vous êtes expert ?">Vous êtes expert ?</a></li>-->
					<li><a href="<?php the_permalink(638); ?>" title="Contact">Actualités</a></li>
					<li><a href="<?php the_permalink(29); ?>" title="Contact">Contact</a></li>
					<!--<li><a href="<?php the_permalink(31); ?>" title="Espace client">Espace client</a></li>-->
					<!--<li><a href="javascript:void(0);" title="Page indisponible" class="isDisabled">Espace client</a></li>-->
				</ul>
			</div>
		</div>
	</div>
	<div class="footer-bottom">
		<div class="container">
			<div class="copyright">
				<?php the_field('copyright', 'option'); ?>
			</div>
			<div class="maki">
				<div>Réalisé par <a href="http://maki-agency.mg" target="_blank">Maki Agency</a></div>
			</div>
			<div class="empty"></div>
		</div>
	</div>
</footer>

<?php wp_footer(); ?>

</main>
</div>
</body>
</html>
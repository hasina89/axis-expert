<?php 
/**
 * Template Name: Page devenir expert
 */
 ?>

<?php get_header(); ?>
<main>
	<?php while ( have_posts() ) : the_post();?>
    <section class="blcCareer blc_page">
        <div class="container">
            <div class="introText">
                <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms"><?php the_field('introduction'); ?></h2>
                <div class="wow fadeInUp" data-wow-delay="800ms">
                    <p><?php the_field('text'); ?></p>
                </div>
            </div>

            <?php /* ?>
	        <?php if( have_rows('listes_carriere') ): ?>
	        <div class="list_career">
	        	<?php while ( have_rows('listes_carriere') ) : the_row(); ?>
	            <div class="item_career  wow fadeInUp" data-wow-delay="800ms">
	                <div class="inner_item_career">
	                    <div class="txt_career">
	                        <h3><?php the_sub_field('titre'); ?></h3>
	                        <?php the_sub_field('information'); ?>
	                    </div>
	                    <div class="btn_career">
	                        <a href="<?php the_sub_field('lien_offre'); ?>" class="btn_light btn hvr-btn">voir l'offre</a>
	                        <a href="<?php the_sub_field('lien_postuler'); ?>" class="btn_dark btn hvr-btn_dark">postuler</a>
	                    </div>
	                </div>
	            </div>
	            <?php endwhile; ?>
	        </div>
	        <?php endif; ?>

            <?php */ ?>

            <?php
            $args = array(
                'post_type' => 'expert',
                'post_status' => 'publish',
                'posts_per_page' => '-1',
                'order' => 'ASC'
            );

            $career_loop = new WP_Query( $args );
            if ( $career_loop->have_posts() ) :
                ?>
                <div class="list_career">
                    <?php while ( $career_loop->have_posts() ) : $career_loop->the_post(); ?>
                        <div class="item_career  wow fadeInUp" data-wow-delay="800ms">
                            <div class="inner_item_career">
                                <div class="txt_career">
                                    <h3><?php the_title(); ?></h3>
                                    <p>Valable jusqu’au <?php the_field('date_dexpiration'); ?></p>
                                    <p>Lieu : <?php the_field('lieu'); ?></p>
                                </div>
                                <div class="btn_career">
                                    <a href="<?php the_permalink(); ?>" class="btn_light btn hvr-btn">voir l'offre</a>
                                    <a href="mailto:info@axis-experts.be?subject=<?php the_title(); ?>" class="btn_dark btn hvr-btn_dark">postuler</a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                </div>
            <?php endif; ?>

        </div>
    </section>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>
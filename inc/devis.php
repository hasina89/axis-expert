<?php 

add_action( 'wp_ajax_devis', 'devis' );
add_action( 'wp_ajax_nopriv_devis', 'devis' );
function devis(){
    if (isset($_POST) && !empty($_POST)) :
        parse_str($_POST['data'], $Data);
        extract($Data);
        $options = array();
        // $status         = $_POST['status'];
        $type_mission   = $Data['type_mission'];
        // $type_immeuble  = $_POST['type_immeuble'];
        // $nb_chambre     = $_POST['nb_chambre'];
        // $nb_salle_bain  = $_POST['nb_salle_bain'];
        // $surface        = $_POST['surface'];
        // $options        = $_POST['options'];
        // $comment        = $_POST['comment'];
        // $adresse        = $_POST['adresse'];
        // $date_heure     = $_POST['date_heure'];
        // $name           = $_POST['name'];
        // $email          = $_POST['email'];
        // $form_type      = $_POST['form_type'];
        $options = $Data['options'];

        if ( $nom == '' ){
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Veuillez entrer votre nom.</p>';
            echo json_encode($data);
            die();
        }

        if ( $email == '' || !filter_var($email, FILTER_VALIDATE_EMAIL) ){
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Veuillez entrer une adresse email valide.</p>';
            echo json_encode($data);
            die();
        }

        if ( $adresse == '' ){
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Veuillez entrer l\'adresse de votre bien.</p>';
            echo json_encode($data);
            die();
        }

        $dd_perso = false;
        // Function to change sender name
        function wpb_sender_name( $original_email_from ) {
            return 'Axis Experts';
        }
        add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
        
        $titre = get_field('titre_popup_devis','option');        
        $conf = get_field('popup_devis','option');
        $titre_perso = get_field('titre_popup_devis_perso','option');
        $conf_perso = get_field('popup_devis_perso','option');

        $destinataire = $email;
        $cc_email = get_field('mail', 'option'); // info@axis-experts.be
        $headers = array("From: $sender_email", 'Content-Type: text/html; charset=UTF-8;MIME-Version: 1.0');
        $headers[] = "Cc: Propriétaire Site <$sender_email>";

        $data = array();

        $cp_part = check_if_email_partenaire( $email );
        
        //si email == email partenaire
        if ( count( $cp_part ) ){
            $reduction = (int)$cp_part['reduction'];
        }elseif( !count( $cp_part ) && $code_promo != '' ){
            $reduction = check_code_promo_public( $code_promo );
            $reduction = $reduction['reduction'];
        }else{
            $reduction = 0;
        }

        if ( 
            ( $type_mission == '402' && ( $type_immeuble == '437' || $type_immeuble == '440' || $type_immeuble == '446' || $type_immeuble == '449' ) ) ||
            ( $type_mission == '403' && ( $type_immeuble == '437' || $type_immeuble == '440' || $type_immeuble == '443' || $type_immeuble == '446'|| $type_immeuble == '449' ) ) ||
            ( $type_mission == '404' && ( $type_immeuble == '437' || $type_immeuble == '440' || $type_immeuble == '443' || $type_immeuble == '446'|| $type_immeuble == '449' || $type_immeuble == '679' ) ) ||
            ( $type_mission == '405' && ( $type_immeuble == '437' || $type_immeuble == '440' || $type_immeuble == '443' || $type_immeuble == '446'|| $type_immeuble == '449' || $type_immeuble == '679' || $type_immeuble == '680' || $type_immeuble == '681' ) ) ||
            ( ( $type_mission == '399' || $type_mission == '401' ) && $type_immeuble == '446' )
        ){
            $subject = get_field('dd_titre_copie','option');
            $subject_admin = get_field('dd_titre_admin_copie','option');
            $template_email = locate_template( 'email/template-devis-perso.php', false, false );
            $dd_perso = true;

            $tm = get_post_info($type_mission);
            ob_start();
                include($template_email);
            $body_mail = ob_get_clean();

            if(@wp_mail( $destinataire, $subject . ' : '. $adresse, $body_mail, $headers ) && @wp_mail($cc_email, $subject_admin . ' : '. $adresse, $body_mail, $headers )){
                $data['result'] = 1; 
                $data['msg'] = '<h2 class="title_success">'.$titre_perso.'</h2><p class="success">'.$conf_perso.'</p>';
            }else {
                $data['result'] = 0;
                $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Une erreur s\'est produite, veuillez reconfirmer SVP.</p>';
            }

                echo json_encode($data);
            die();

        }else{
            // Check option
            $avec_options = get_field('avec_options', $type_mission); 

            // Options supp
            $all_options = array();
            foreach ($options as $option) {
                $all_options[] = get_options($option, $type_mission);
            }

            $total_prix_options = get_all_option_price($all_options); 
            if ( $type_mission == '399' || $type_mission == '401' ){
                if ( $type_immeuble == '437' || $type_immeuble == '440' || $type_immeuble == '446' || $type_immeuble == '449' ){
                    $surface_only = true;
                }elseif( $type_immeuble == '443' ){
                    $meuble_only  = true;
                }
            }             
            
            // Prix total du bien
            $prix_bien = get_prix_bien($type_mission, $type_immeuble, $nb_chambre, $surface );

            // Total Price
            if ( ($type_mission == '402' && $type_immeuble == '406') ||
                 ($type_mission == '402' && $type_immeuble == '418') ||
                 ($type_mission == '403' && $type_immeuble == '406') ||
                 ($type_mission == '403' && $type_immeuble == '418') ||
                 ($type_mission == '404' && $type_immeuble == '406') ||
                 ($type_mission == '404' && $type_immeuble == '418') ||
                 ($type_mission == '404' && $type_immeuble == '681') ||
                 ($type_mission == '404' && $type_immeuble == '680') ||
                 ($type_mission == '405' && $type_immeuble == '406') ||
                 ($type_mission == '405' && $type_immeuble == '418') ||
                 ($type_mission == '399' && $type_immeuble == '443') ||
                 ($type_mission == '401' && $type_immeuble == '443') ){
                $grand_total = $prix_bien;
            }else{
                $grand_total = get_total_price($total_prix_options, $prix_bien);
            }

            if ( $reduction > 0 ){
                
                $reduit = $reduction * $grand_total / 100;               
            }

            $subject = get_field('dd_titre','option');//'Demande de devis';
            $subject_admin = get_field('dd_titre_admin','option');//'Demande de Rendez-vous';
            //$template_email = locate_template( 'email/devis.php', false, false );
            $template_email = locate_template( 'email/template-devis.php', false, false );
            
        // }
        $tm = get_post_info($type_mission);
        ob_start();
            include($template_email);
        $body_mail = ob_get_clean();             

            if(@wp_mail( $destinataire, $subject . ' : '. $adresse, $body_mail, $headers ) && @wp_mail($cc_email, $subject_admin . ' : '. $adresse, $body_mail, $headers )){
                $data['result'] = 1; 
                $data['msg'] = '<h2 class="title_success">'.$titre.'</h2><p class="success">'.$conf.'</p>';
            }else {
                $data['result'] = 0;
                $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Une erreur s\'est produite, veuillez reconfirmer SVP.</p>';
            }
        echo json_encode($data);
            die();
       }
        
    endif;
}

add_action( 'wp_ajax_finaliser_devis', 'finaliser_devis' );
add_action( 'wp_ajax_nopriv_finaliser_devis', 'finaliser_devis' );

function finaliser_devis(){

    $type_immeuble  = $_POST['type_immeuble'];
    $status         = $_POST['status'];
    $type_mission   = $_POST['type_mission'];
    $type_immeuble  = $_POST['type_immeuble'];
    $nb_chambre     = $_POST['nb_chambre'];
    $nb_salle_bain  = $_POST['nb_salle_bain'];
    $surface        = $_POST['surface'];
    $options        = $_POST['options'];
    $comment        = $_POST['comment'];
    $adresse        = $_POST['adresse'];
    $date_devis     = $_POST['date_devis'];
    $heure_devis     = $_POST['heure_devis'];
    $nom            = $_POST['name'];
    $email          = $_POST['email'];
    $honoraire      = $_POST['honoraire'];
    $reduction      = (int)$_POST['reduction'];
    if ( $reduction === undefined ){
        $reduction = 0;
    }
    $reduit = $reduction * $honoraire / 100; 
    
    $attachs = array();

    function wpb_sender_name( $original_email_from ) {
            return 'Axis Experts';
    }
    add_filter( 'wp_mail_from_name', 'wpb_sender_name' );

    if ( isset($_FILES)){
            if (!function_exists('wp_handle_upload')) {
                    require_once(ABSPATH . 'wp-admin/includes/file.php');
            }
            
            $upload_overrides = array('test_form' => false);

            $files = $_FILES['files'];
            foreach ($files['name'] as $key => $value) {
              if ($files['name'][$key]) {
                $file = array(
                  'name'     => $files['name'][$key],
                  'type'     => $files['type'][$key],
                  'tmp_name' => $files['tmp_name'][$key],
                  'error'    => $files['error'][$key],
                  'size'     => $files['size'][$key]
                );
                $movefile = wp_handle_upload($file, $upload_overrides); 
                if ($movefile && !isset($movefile['error'])) {
                     $attachs[] = $movefile['file'];
                } else {
                    echo $movefile['error']. '</br>';
                }
              }
            }
        }  

    $titre = get_field('titre_popup_devis_fin','option');        
    $conf = get_field('popup_devis_fin','option');

    $destinataire = $email;
    $cc_email = get_field('mail', 'option'); // info@axis-experts.be
    $headers = array("From: $sender_email", 'Content-Type: text/html; charset=UTF-8;MIME-Version: 1.0');
    $headers[] = "Cc: Propriétaire Site <$sender_email>";

    $subject = get_field('dd_titre_fin','option');
    $subject_admin = get_field('dd_titre_admin_fin','option');
    $template_email = locate_template( 'email/template-devis-finaliser.php', false, false );

    $data = array();

    ob_start();
        include($template_email);
    $body_mail = ob_get_clean();            

        if(@wp_mail( $destinataire, $subject_admin . ' : '. $adresse, $body_mail, $headers, $attachs ) ){
            @wp_mail($cc_email, $subject_admin . ' : '. $adresse, $body_mail, $headers, $attachs);
            $data['result'] = 1; 
            $data['msg'] = '<h2 class="title_success">'.$titre.'</h2><p class="success">'.$conf.'</p>';
        }else {
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Une erreur s\'est produite, veuillez reconfirmer SVP.</p>';
        }
    echo json_encode($data);
    die();
}
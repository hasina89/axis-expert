<?php 

add_action( 'wp_ajax_rdv', 'rdv' );
add_action( 'wp_ajax_nopriv_rdv', 'rdv' );

function rdv(){
    if (isset($_POST) && !empty($_POST)) :
        parse_str($_POST['data'], $Data);
        extract($Data);
        $options = array();

        $status         = $_POST['status'];
        $type_mission   = $_POST['type_mission'];
        $type_immeuble  = $_POST['type_immeuble'];
        $nb_chambre     = $_POST['nb_chambre'];
        $nb_salle_bain  = $_POST['nb_salle_bain'];
        $surface        = $_POST['surface'];
        $comment        = $_POST['comment'];
        $adresse        = $_POST['adresse'];
        $numeroAppart        = $_POST['numeroAppart'];
        $date_rdv        = $_POST['date_rdv'];
        $heure_rdv       = $_POST['heure_rdv'];
        $nom           = $_POST['name'];
        $email          = $_POST['email'];
        $form_type      = $_POST['form_type'];
        $numero      = $_POST['numero'];
        $options        = explode(',', $_POST['options']);

        if ( $adresse == '' ){
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Veuillez entrer l\'adresse de votre bien.</p>';
            echo json_encode($data);
            die();
        }

        if ( isset($_FILES)){
            if (!function_exists('wp_handle_upload')) {
                    require_once(ABSPATH . 'wp-admin/includes/file.php');
            }
            $attachs = array();
            $upload_overrides = array('test_form' => false);

            $files = $_FILES['files'];
            foreach ($files['name'] as $key => $value) {
              if ($files['name'][$key]) {
                $file = array(
                  'name'     => $files['name'][$key],
                  'type'     => $files['type'][$key],
                  'tmp_name' => $files['tmp_name'][$key],
                  'error'    => $files['error'][$key],
                  'size'     => $files['size'][$key]
                );
                $movefile = wp_handle_upload($file, $upload_overrides); 
                if ($movefile && !isset($movefile['error'])) {
                     $attachs[] = $movefile['file'];
                } else {
                    echo $movefile['error']. '</br>';
                }
              }
            }
        }        

        $dd_perso = false;
        $missions_immeubles = get_field('prix_mission_immeuble','option');
        $prix = array();
        $out = array();

        // Check option
        $avec_options = get_field('avec_options', $type_mission); 

        // Options supp
        $all_options = array();
        $situation = 'Non Meubl&#233;';
        foreach ($options as $option) {
            $all_options[] = get_options($option, $type_mission);
            if ($option['name'] == "Meublé")
                $situation = "Meubl&#233;";
        }
        
        $total_prix_options = get_all_option_price($all_options);          
        
        // Prix total du bien
        $prix_bien = get_prix_bien($type_mission, $type_immeuble, $nb_chambre, $surface );      

        // Total Price
        $grand_total = get_total_price($total_prix_options, $prix_bien);        
        if ( $type_mission == '399' || $type_mission == '401' ){
            if ( $type_immeuble == '437' || $type_immeuble == '440' || $type_immeuble == '446' || $type_immeuble == '449' ){
                $surface_only = true;
                $avec_options = 'non';
            }elseif( $type_immeuble == '443' ){
                $meuble_only  = true;
                $avec_options = 'oui';
            }
        }

        $subject = get_field('rdv_titre','option');//'Demande de Rendez-vous';
        $subject_admin = get_field('rdv_titre_admin','option');//'Demande de Rendez-vous';
        //$template_email = locate_template( 'email/rendezvous.php', false, false );
        $template_email = locate_template( 'email/template-rdv.php', false, false );
        $titre = get_field('titre_popup_rdv','option'); 
        $conf = get_field('popup_rdv','option');
        $tm = get_post_info($type_mission);
        ob_start();
            include($template_email);
        $body_mail = ob_get_clean();
             
        // Function to change sender name
        function wpb_sender_name( $original_email_from ) {
            return 'Axis Expert';
        }
        add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
        
        
        $destinataire = $_POST['email'] ;
        $cc_email = get_field('mail', 'option'); // info@axis-experts.be
        $headers = array("From: Axis Expert", 'Content-Type: text/html; charset=UTF-8;MIME-Version: 1.0');

        $data = array();
        
        if(@wp_mail( $_POST['email'], $subject . ' : '. $adresse,  $body_mail , $headers, $attachs ) ){
            @wp_mail($cc_email, $subject_admin . ' : '. $adresse, $body_mail, $headers, $attachs );
            $data['result'] = 1; 
            $data['msg'] = '<h2 class="title_success">'.$titre.'</h2><p class="success">'.$conf.'</p>';
        }else {
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Une erreur s\'est produite, veuillez reconfirmer SVP.</p>';
        }
        
        
        echo json_encode($data);
        die();
       
        
    endif;
}


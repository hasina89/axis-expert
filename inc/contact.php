<?php 
add_action( 'wp_ajax_contact', 'contact' );
add_action( 'wp_ajax_nopriv_contact', 'contact' );
function contact(){
    if (isset($_POST) && !empty($_POST)) :
        parse_str($_POST['data'], $Data);
        extract($Data); 
        
        $responseCaptcha = $Data['g-recaptcha-response'];         
        $errorMsgCaptcha = "Veuillez vérifier le captcha anti-robot";
        
        $data = array();

		// Email
		$subject = get_field('di_titre','option');//"Demande d'information";
        $subject_admin = get_field('di_titre_admin','option');//'Demande de Rendez-vous';
        ob_start();
            $template_email = locate_template( 'email/template-contact.php', false, false );
            include($template_email);
        $body_mail = ob_get_clean();
             
        // Function to change sender name
        function wpb_sender_name( $original_email_from ) {
            return 'Axis Expert';
        }
        add_filter( 'wp_mail_from_name', 'wpb_sender_name' );
        

        if(isset($responseCaptcha) && !empty($responseCaptcha)){ 
            $secret = '6LfD570UAAAAAC_h8vSxO3kPdjMqG3NyOiO16Leo';
            $url_captcha = 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$responseCaptcha.'';          
            // $verifyResponse = preg_match("/^http/", $url_captcha) ? http_get_contents($url_captcha) : file_get_contents($url_captcha);
            // $responseData = json_decode($verifyResponse);   

            // print_r('$responseData');  
            // die();

            //if($responseData->success){
            
                // $destinataire = 'rhfrederic@gmail.com';
                $destinataire = $email;
                $cc_email = get_field('mail', 'option'); // info@axis-experts.be
                $headers = array("From: $sender_email", 'Content-Type: text/html; charset=UTF-8');
                $headers[] = "Cc: Propriétaire Site <$sender_email>;<$cc_email>";
                if(@wp_mail( $destinataire, $subject, $body_mail, $headers ) && @wp_mail($cc_email, $subject_admin, $body_mail, $headers )){
                    $data['result'] = 1; 
                    $data['msg'] = '<h2 class="title_success">Confirmation</h2><p class="success">Votre demande a été prise en compte, <br> Nous vous répondrons dans le plus bref délai.</p>';
                }else {
                    $data['result'] = 0;
                    $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">Une erreur s\'est produite, veuillez reconfirmer SVP.</p>';
                }
            //}
        }
        else { 
            $data['result'] = 0;
            $data['msg'] = '<h2 class="title_error">Erreur</h2><p class="error">'.$errorMsgCaptcha.'</p>';           
        }

        echo json_encode($data);
        die();
       
    	
    endif;
}
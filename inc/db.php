<?php 
	/* Get Mission */
	function get_custom_posts($post_type ='mission'){
		$args = array (
		    'posts_per_page'    => -1,
		    'post_type'         => $post_type,
		    'order'             =>'DESC',
		);
		$Q_all = new WP_Query( $args );
		return $Q_all;
	}

	// Get Customer Step
	function get_leads(){
	    global $wpdb;
	    $quote_table = $wpdb->prefix.'quote';

	    $result = $wpdb->get_results(
	        "SELECT * FROM $quote_table"
	    ); 

	    // print_r($result);

	    if ($result) {
	        return $result;
	    }else {
	        return false;
	    }
	}
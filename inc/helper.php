<?php 
// get 
function get_prix_bien($type_mission, $type_immeuble, $nb_chambre = 0, $surface = 0 ){
	$liste_immeubles = get_field('liste_immeuble', $type_mission);
	if( $liste_immeubles ) {
	    foreach( $liste_immeubles as $liste_immeuble ) {
			$item_immeuble = $liste_immeuble['item_immeuble'];
			$liste_chambres = $liste_immeuble['liste_chambre'];
			$tarif = $liste_immeuble['tarif'];
			if ($item_immeuble == $type_immeuble) {
				switch ($tarif) {
					case 'avec_prix':
						foreach( $liste_chambres as $liste_chambre ){
							$nombre_chambre = $liste_chambre['nombre_chambre'];
							$prix_chambre   = $liste_chambre['prix_chambre'];
							if($nombre_chambre == $nb_chambre){
								if($prix_chambre == -1){
									$prix_bien = "N'existe pas";
								}else {
									$prix_bien = $prix_chambre;
								}
							}						
						}
						break;
					case 'sur_devis':
						$prix_bien = 'Sur Devis';
						break;
					case 'prix_unique':
						$prix_bien = $liste_immeuble['unique_prix'];
						break;
					case 'surface':
						$prix_de_base = $liste_immeuble['prix_de_base'];
						$prix_min_50  = $liste_immeuble['prix_min_50'];
						$prix_max_50  = $liste_immeuble['prix_max_50'];
						if($surface <= 50 ){
							$prix_bien = $prix_de_base + (( $surface - 50 ) * $prix_min_50);
						}else {
							$prix_bien = $prix_de_base + (( $surface - 50 ) * $prix_max_50);								
						}
						break;
					default:
						$prix_bien = 'Aucun';
						break;
				}					
			}
            
	    }
	}
	return $prix_bien;
}
// get all options
function get_options($id, $type_mission){
	$args = array (
		'p'         => $id,
		'post_type' => 'autre_option',
	);
	$post_option = new WP_Query( $args );
	$array_option = array();
	if(!empty($post_option)){
		$Option_info = $post_option->posts[0];
		$option_id = $Option_info->ID;
		$option_title = $Option_info->post_title;
		$tarif_chambres = get_field('tarif_chambre', $option_id);
		if( $tarif_chambres ) {
		    foreach( $tarif_chambres as $tarif_chambre ) {
				$liste_mission_option = $tarif_chambre['liste_mission_option'];
				$prix_option          = $tarif_chambre['prix_option'];
				if( $liste_mission_option == $type_mission){
					$prix_option_supp = $prix_option;
				}
			}
		}
		$array_option['name'] = $option_title;
		$array_option['prix'] = $prix_option_supp;
	}
	
	return $array_option;
	
}
// get Option price 
function get_all_option_price($options){
	$prix = 0;
	foreach ($options as $option) {
		$prix += $option['prix'];
	}
	return $prix;
}
// get total price 
function get_total_price($options, $biens){
	$prix = floatval($options) + floatval($biens);
	return $prix;
}
// Get post info
function get_post_info( $id, $field='post_title' ){
	$post = get_post( $id );
	return $post->$field;
}

function get_tarification( $tarif ){
	switch ($tarif) {
		case 'avec_prix':
			$resp = 'Par chambre';
			break;
		case 'sur_devis':
			$resp = 'Sur devis';
			break;
		case 'surface':
			$resp = 'Selon la surface';
			break;		
		default:
			$resp = 'Prix unique';
			break;
	}

	return $resp;
}
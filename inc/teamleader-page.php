<?php /* Template name: Traitement Teamleader */

// $a = print_r($_GET);
// echo json_encode( $_GET );

/*echo 'ici';
wp_redirect( 'http://www.google.fr' );
die();
*/
$code = rawurldecode($_GET['code']);

$clientId = 'a67bac7cd4889be11f6eadb60435a6f8';
$clientSecret = '2ff4356f814291cfa81e87027ae380e4';

global $wp;
$redirectUri = home_url( $wp->request );

/**
 * Request an access token based on the received authorization code.
 */
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, 'https://app.teamleader.eu/oauth2/access_token');
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, [
    'code' => $code,
    'client_id' => $clientId,
    'client_secret' => $clientSecret,
    'redirect_uri' => $redirectUri,
    'grant_type' => 'authorization_code',
]);

$response = curl_exec($ch);
$data = json_decode($response, true);
$accessToken = $data['access_token'];

$az = array('accessToken' => $accessToken);

echo json_encode( $az );
die();  




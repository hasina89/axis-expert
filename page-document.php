<?php 
/**
 * Template Name: Page document
 */
 ?>

<?php get_header(); ?>
<main>
	<?php while ( have_posts() ) : the_post();?>
    <section class="blcCareer blc_page">
        <div class="container">
            <div class="introText">
                <h2 class="introHeading wow fadeInUp" data-wow-delay="800ms"><?php the_field('introduction'); ?></h2>
                <div class="wow fadeInUp" data-wow-delay="800ms">
                    <p><?php the_field('text'); ?></p>
                </div>
            </div>

			<?php if( have_rows('listes_document') ): ?>
				<div class="catDoc">
				<?php while ( have_rows('listes_document') ) : the_row(); ?>
					<h3 class="title_cat_doc"><?php the_sub_field('titre_section'); ?></h3>
					<div class="list_doc">
					<?php if( have_rows('sections_document') ): ?>
						<?php while ( have_rows('sections_document') ) : the_row(); ?>
                            <div class="item_doc wow <?php the_sub_field('style_animation'); ?>" data-wow-delay="800ms">
                                <div class="inner_item_doc">
                                    <div class="txt_doc">
                                        <h3><?php the_sub_field('titre_document'); ?></h3>
                                    </div>
                                    <div class="btn_doc">
                                        <a href="<?php the_sub_field('fichier'); ?>" target="_blank" class="btn_light btn hvr-btn ">Télécharger</a>
                                    </div>
                                </div>
                            </div>
						<?php endwhile; ?>
					<?php endif; ?>
					</div>
				<?php endwhile; ?>
				</div>
			<?php endif; ?>

        </div>
    </section>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
    <link rel="shortcut icon" href="<?php the_field('favicon', 'option'); ?>" type="image/x-icon"/>
    <?php wp_head(); ?>
    <!-- Smartsupp Live Chat script -->
    <!-- Smartsupp Live Chat script -->
    <script type="text/javascript">
        var _smartsupp = _smartsupp || {};
        _smartsupp.key = '217e30d4458a080b0a1b138a1d6bb113041ec756';
        window.smartsupp || (function (d) {
            var s, c, o = smartsupp = function () {
                o._.push(arguments)
            };
            o._ = [];
            s = d.getElementsByTagName('script')[0];
            c = d.createElement('script');
            c.type = 'text/javascript';
            c.charset = 'utf-8';
            c.async = true;
            c.src = 'https://www.smartsuppchat.com/loader.js?';
            s.parentNode.insertBefore(c, s);
        })(document);
    </script>

    <script>
        var theme_url = "<?= get_template_directory_uri(); ?>";
    </script>

    <style>
        .banner {
            <?php if(get_field('banner')): ?>

            background: url("<?php the_field('banner') ?>") center no-repeat !important;
            background-size: cover !important;

            <?php endif; ?>
        }
    </style>

</head>

<?php if (is_front_page()) : ?>
<body>
<?php else : ?>
<body <?php body_class('page'); ?>>
<?php endif; ?>
<div id="wrapper">
    <main>
        <!-- HEADER START -->
        <header id="header">
            <div class="header banner">
                <div class="blcTop">
                    <div class="blcMenu container wow fadeInDown" data-wow-delay="800ms">
                        <div class="wrapMenuMobile">
                            <div class="menuMobile">
                                <div></div>
                                <span>MENU</span></div>
                        </div>
                        <?php wp_nav_menu(array('theme_location' => 'primaire', 'container' => 'nav', 'container_class' => 'menu', 'menu_class' => ' ')); ?>
                        <a href="<?php echo esc_url(home_url('/')); ?>" class="logo" title="Axis Expert"><img src="<?php the_field('logo', 'option'); ?>"></a>
                    </div>
                </div>
                <div class="content-banner">
                    <div class="table-cell">
                        <div class="container">
                            <div class="textBanner">
                                <?php if (is_front_page()) : ?>
                                    <div class="title wow fadeInUp" data-wow-delay="800ms"><?php the_field('petit_titre'); ?></div>
                                    <h1 class="titre wow fadeInUp" data-wow-delay="1000ms"><?php the_field('titre'); ?></h1>
                                    <div class="wow fadeInUp" data-wow-delay="1200ms">
                                        <p>
                                            <?php the_field('text'); ?>
                                        </p>
                                    </div>
                                <?php else : ?>
                                    <h1 class="titre wow fadeInUp" data-wow-delay="1000ms"><?php the_title(); ?></h1>
                                <?php endif; ?>
                            </div>
                            <div class="blcButton">
                                <ul class="wow fadeInUp" data-wow-delay="1600ms">
                                    <li>
                                        <a href="<?php the_permalink(79); ?>" class="btn rdv hvr-btn" title="Je prends rendez-vous"><span>Je prends rendez-vous</span></a>
                                    </li>
                                    <li>
                                        <a href="tel:+3228809090" class="btn exp hvr-btn" title="Nous contacter"><span>Nous contacter</span></a>
                                    </li>
                                    <li>
                                        <a href="<?php the_permalink(82); ?>" class="btn prix hvr-btn" title="Devis online"><span>Devis online</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="blcAvis">
                    <a href="#blcPopup" class="on fancybox" data-fancybox="groupe1">
                        <span class="txtAvis">Rédiger votre avis <span>en cliquant ici</span></span>
                    </a>
                    <div class="off">
                        <span class="txtAvis">Rédiger votre avis</span>
                    </div>
                </div>
                <div class="blcSociaux">
                    <div class="fb">
                        <span class="content-rs"><a target="_blank" href="<?php echo get_field('facebook', 'option') ?? "#" ?>">suivez-nous</a></span> <!-- https://www.facebook.com/ -->
                    </div>

                    <!--
                                    <div class="twit">
                                        <span class="content-rs"><a target="_blank" href="<?php echo get_field('twitter', 'option') ?? "#" ?>">suivez-nous</a></span>
                                    </div>
                                    -->

                    <div class="linkd">
                        <span class="content-rs"><a target="_blank" href="<?php echo get_field('linkedin', 'option') ?? "#" ?>">suivez-nous</a></span> <!-- https://www.linkedin.com/ -->
                    </div>
                </div>

                <?php if (is_front_page()) : ?>
                    <!--<a href="<?php the_permalink(301); ?>" class="btnChantier">-->
                    <a href="mailto:chantiers@axis-experts.be" class="btnChantier">
                        <span class="titleBtn"><em>besoin d’états des lieux</em> pour vos chantiers ?</span>
                    </a>
                <?php endif; ?>
            </div>
            <div class="blcPopup" id="blcPopup" style="display: none">
                <div class="titre-pp">
                    <span>rédigez votre avis</span>
                </div>
                <div class="content-pp">
                    <div class="formulaire">
                        <?php echo do_shortcode('[contact-form-7 id="279" title="Avis client"]'); ?>
                    </div>
                </div>
            </div>
        </header>
        <!-- HEADER END -->